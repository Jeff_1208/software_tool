package service;

import java.util.ArrayList;
import java.util.List;

public class ServicePN {
	private String PN;
	private String PUID;
	private String DESC;
	private ArrayList<String> Rule;
	private List<ArrayList<String>> Rule_conditions;
	
	public ArrayList<String> getRule() {
		return Rule;
	}
	public void setRule(ArrayList<String> rule) {
		Rule = rule;
	}
	
	public String getPN() {
		return PN;
	}
	public void setPN(String pN) {
		PN = pN;
	}
	public String getDESC() {
		return DESC;
	}
	public void setDESC(String dESC) {
		DESC = dESC;
	}
	public String getPUID() {
		return PUID;
	}
	public void setPUID(String pUID) {
		PUID = pUID;
	}
	public List<ArrayList<String>> getRule_conditions() {
		return Rule_conditions;
	}
	public void setRule_conditions(List<ArrayList<String>> rule_conditions) {
		Rule_conditions = rule_conditions;
	}
}
