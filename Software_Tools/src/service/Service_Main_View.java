package service;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Service_Main_View extends JFrame implements ActionListener{

	private static final long serialVersionUID = 1L;

	static String Select_path;
	private JTextField CSR_Field;
	private JPanel mainpanel;
	private JPanel CTO_Name_panel;
	private JPanel showpanel;
	private JButton scan_btn_CSR;
	private JButton scan_btn_Loadsheet;
	private JButton run_btn;
	private JTextArea show_unusual_FC;

	static String CSR_path;
	public static String Update_Folder="Update";
	//	static String Source_CTO_Name;
	//	static String New_CTO_Name;
	//	static String Source_MT_Name;
	//	static String New_MT_Name;
	//	static String Source_Group_Start_MT;
	//	static String Source_Group_Start_CTO;
	//	static String New_Group_Start_MT;
	//	static String New_Group_Start_CTO;

	JRadioButton randioButton1;
	JRadioButton randioButton2;
	JRadioButton randioButton3;
	JRadioButton randioButton4;

	public Service_Main_View() {

		show_unusual_FC= new JTextArea(10,30);
		show_unusual_FC.setLineWrap(true);  

		CSR_Field = new JTextField(20);

		scan_btn_CSR = new JButton("Find Source File ");
		scan_btn_Loadsheet = new JButton("Find Source Loadsheet ");
		run_btn = new JButton("Start Run");
		run_btn.setSize(10,30);

		randioButton1=new JRadioButton("Get FC List from Rules",true);  
		randioButton2=new JRadioButton("Get PUID for PN"); 
		randioButton3=new JRadioButton("Generate AML Rule");  
		randioButton4=new JRadioButton("Generate SQL create PUID For PN");  
		//product_SQL.generate_CTO_FC_Item(CSR_path);
		//product_SQL.set_map_for_Service(CSR_path);
		// product_SQL.get_PUID_DB_for_Source_File(CSR_path);
		//product_SQL.generate_AML_Code_for_Service(CSR_path);

		ButtonGroup group=new ButtonGroup();  
		group.add(randioButton1);  
		group.add(randioButton2);
		group.add(randioButton3);
		group.add(randioButton4);

		CTO_Name_panel = new JPanel();
		CTO_Name_panel.setLayout(new GridLayout(7,1,1,1)) ;
		CTO_Name_panel.add(scan_btn_CSR);
		CTO_Name_panel.add(CSR_Field);
		CTO_Name_panel.add(randioButton1);
		CTO_Name_panel.add(randioButton2);
		CTO_Name_panel.add(randioButton3);
		CTO_Name_panel.add(randioButton4);
		CTO_Name_panel.add(run_btn);

		showpanel = new JPanel();
		mainpanel = new JPanel();
		mainpanel.setLayout(new BorderLayout());
		showpanel.setLayout(new BorderLayout());
		showpanel.add(show_unusual_FC);
		mainpanel.add(CTO_Name_panel,BorderLayout.NORTH); 
		mainpanel.add(showpanel,BorderLayout.SOUTH); 
		setTitle("Service Tool");

		this.add(mainpanel, BorderLayout.CENTER); 
		this.setSize(600, 1000);
		this.setVisible(true);
		scan_btn_CSR.addActionListener(this);
		scan_btn_Loadsheet.addActionListener(this);
		run_btn.addActionListener(this);
		this.addWindowListener(new WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});

		Toolkit too = Toolkit.getDefaultToolkit() ;
		Dimension screenSize = too.getScreenSize() ;
		this.setLocation((screenSize.width-WIDTH)/2, (screenSize.height-HEIGHT)/2) ;
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE) ;
		this.setResizable(true); 
		this.pack();
	}


	public static void main(String[] args) {
		new Service_Main_View();

	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if(e.getSource()==scan_btn_CSR){
			JFileChooser chooser = new JFileChooser();
			chooser.setMultiSelectionEnabled(false);

			chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			chooser.setDialogTitle("Find Source File");


			int result = chooser.showOpenDialog(this);
			if (result == JFileChooser.APPROVE_OPTION) {
				File file = chooser.getSelectedFile();
				String filepath = file.getAbsolutePath();
				CSR_Field.setText( filepath);// 将文件路径设到JTextField
				CSR_path=filepath;
			}
		}else if(e.getSource()==run_btn){
			Service_Utils service_Utils = new Service_Utils(CSR_path);
			if(randioButton1.isSelected()){
				ArrayList<String> New_condition_FC_CTO_List=service_Utils.generate_CTO_FC_Item(CSR_path);
				//service_Utils.set_map_for_Service(CSR_path);
				show_unusual_FC.setText("Please find the Condition FC/CTO from D:/1_Service/Service_Condition_FC_CTO.txt");
				
				String FC_CTO_List ="";
				if(New_condition_FC_CTO_List.size()>0){
					String Select_SQL="";
					for (String FC:New_condition_FC_CTO_List) {
						Select_SQL += "'"+FC.toString() + "',";
					}
					Select_SQL=Select_SQL.substring(0,Select_SQL.length()-1);
					Select_SQL="select PUID,FEAT_CODE from DB2INST1.PRODUCT  where FEAT_CODE in("+Select_SQL.trim()+")";
					
					FC_CTO_List=New_condition_FC_CTO_List.toString().substring(1,New_condition_FC_CTO_List.toString().length()-1);
					show_unusual_FC.setText("Please find the Condition FC/CTO from D:/1_Service/Service_Condition_FC_CTO.txt\n\n"+
					"These Condition FC/CTO as below need to search PUID and update it in source File:\n "+FC_CTO_List+"\n\n"+
				    "Use the SQL to serach PUID for the FC/CTO:\n"+Select_SQL);
				}
				
			}else if(randioButton2.isSelected()){
				String output_path=service_Utils.get_PUID_DB_for_Source_File(CSR_path);
				show_unusual_FC.setText("Please find the file for PUID Define from "+output_path);
			}else if(randioButton3.isSelected()){
				List<String> Duplicate_PNs=service_Utils.generate_AML_Code_for_Service(CSR_path);
				Calendar cale = Calendar.getInstance();  
				int month = cale.get(Calendar.MONTH) + 1;  
				String filepath="";
				if(month<10){
					filepath="D:/1_Service/Service_"+cale.get(Calendar.YEAR)+"0"+month+cale.get(Calendar.DATE)+".txt";
				}else{
					filepath="D:/1_Service/Service_"+cale.get(Calendar.YEAR)+month+cale.get(Calendar.DATE)+".txt";
				}
				if(Duplicate_PNs.size()>0){
					show_unusual_FC.setText("Please find the file for FilterItem from "+filepath+"\n\n"+
					"These is have some FCs as below are duplicate in file:\n "+Duplicate_PNs.toString()+"\n\n");
				}else{
					show_unusual_FC.setText("Please find the file for FilterItem from "+filepath+"\n");
				}
			}else if(randioButton4.isSelected()){
				service_Utils.Generate_SQL_creat_PUID_for_Service(CSR_path);
				show_unusual_FC.setText("Please find the file for update Description from D:/1_Service/Service_SQL_Create_PUID.txt");
			}
		}

		//String unusual_FC_message="Please find the CTO Loadsheet from:\n"+CSR_path+"//"+Update_Folder;
		//show_unusual_FC.setText(unusual_FC_message);
	}

}

