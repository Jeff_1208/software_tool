package service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import common.Common_Utils;

public class Service_Rule_Utils {
	
	@SuppressWarnings("deprecation")
	public List<String> generate_AML_Code_for_Service(String path){
		String except_FCs="B470,B471,B472,B473,B474,Japan,China,-";
		List<ServicePN> servicePNs= new ArrayList<ServicePN>();
		
		
		List<String> ALL_PNs= new ArrayList<String>();
		List<String> Duplicate_PNs= new ArrayList<String>();
		XSSFWorkbook xssfWorkbook;

		//String dist_file_path = Folder_path+"\\Update\\"+filename;
		//System.out.println("path: "+source_file_path);
		try {
			xssfWorkbook = new XSSFWorkbook(path);

			XSSFSheet xssfSheet = xssfWorkbook.getSheet("Services");
			if(xssfSheet==null){
				xssfSheet = xssfWorkbook.getSheetAt(0);
			}
			if(xssfSheet==null){
				System.out.println("The Sheet is not Exist");
			}

			//ArrayList<String>  ss = new ArrayList<>();
			int eof_row= Common_Utils.getEOF_Row(xssfSheet, xssfSheet.getLastRowNum()+1);
			//Map<String, String>  CTO_FC_Map = get_map();
			Map<String, String>  CTO_FC_Map = get_condition_FC_CTO_map_for_Service(path);
			int count=0;
			int rule_count=0;
			StringBuffer stringBuffer = new StringBuffer();
			
			for(int i=1;i<eof_row;i++){
				
				ServicePN servicePN = new ServicePN();
						
				XSSFRow row =xssfSheet.getRow(i);
				if(row==null) continue;
				
				XSSFCell PUID_cell = row.getCell(0);
				if(PUID_cell==null) continue;
				String PUID_String=PUID_cell.toString();
				
				XSSFCell PN_cell = row.getCell(1);
				String PN_String=PN_cell.toString();
				
				XSSFCell DESC_cell = row.getCell(2);
				String DESC_String=DESC_cell.toString();
				
				XSSFCell Rule_cell = row.getCell(3);
				String Rule_String_raw=Rule_cell.toString();
				String Rule_String = Rule_String_raw.replaceAll(" " , "");

				int Deriveon_end_index_raw=Rule_String_raw.lastIndexOf("Derive");
				int Deriveon_start_index=Rule_String.indexOf("Deriveon");
				int Deriveon_end_index=Rule_String.lastIndexOf("Deriveon");

				ArrayList<String> rule = new ArrayList<>();
				List<ArrayList<String>> Rule_conditions = new ArrayList<ArrayList<String>>();
				
				boolean two_conditions= false;
				if(Deriveon_start_index!=Deriveon_end_index){
					two_conditions= true;
					count++;
				}
				
				if(two_conditions){
					String rule1_string= Rule_String.substring(0, Deriveon_end_index);
					String rule2_string= Rule_String.substring(Deriveon_end_index);
					
					Rule_conditions.add(extractMessage(rule1_string));
					Rule_conditions.add(extractMessage(rule2_string));
					
					String rule1_string_raw= Rule_String_raw.substring(0, Deriveon_end_index_raw);
					String rule2_string_raw= Rule_String_raw.substring(Deriveon_end_index_raw);	
			
					rule.add(rule1_string_raw);
					rule.add(rule2_string_raw);
				}else if(!two_conditions){
					rule.add(Rule_String_raw);
					Rule_conditions.add(extractMessage(Rule_String));
				}
				servicePN.setPUID(PUID_String);
				servicePN.setPN(PN_String);
				servicePN.setDESC(DESC_String);
				servicePN.setRule(rule);
				servicePN.setRule_conditions(Rule_conditions);

				if(ALL_PNs.contains(PN_String)){
					ALL_PNs.add(PN_String);
					servicePNs.add(servicePN);
				}else{
					Duplicate_PNs.add(PN_String);
				}
				
				if(!two_conditions){
					rule_count++;
					List<String> list = extractMessage(Rule_String);
					//System.out.println("//"+PUID_String+"\t"+PN_String+"\t"+DESC_String);
					//System.out.println("//"+Rule_String_raw);
					stringBuffer.append("//"+PUID_String+"\t"+PN_String+"\t"+DESC_String+"\n");
					stringBuffer.append("//"+Rule_String_raw+"\n");


					DESC_String = DESC_String.replaceAll(" " , "");
					DESC_String = DESC_String.replaceAll("-" , "");
					String CTO_PN_String="";
					String CTO_Puid="";
					String sourcePuids="";
					for (int j = 0; j < list.size(); j++) {
						String value= list.get(j);
						if(value.contains("7Y")&&value.length()>5){
							CTO_Puid=CTO_FC_Map.get(value);
							CTO_PN_String=PN_String+"_"+value;
						}
						if(except_FCs.contains(value)) continue;
						if((!value.contains("Japan")&&(!value.contains("China")))){
							sourcePuids+="1x"+CTO_FC_Map.get(value)+",";
						}
					}
					if(sourcePuids.length()<2)
					{
						System.out.println(PUID_String+" "+PN_String);
					}
					String rule_name ="autoAdd "+CTO_PN_String+"_SVC_WW extends ConditionalAdd {";
					sourcePuids=sourcePuids.substring(0,sourcePuids.length()-1);
					System.out.println(rule_name);
					System.out.println("    sourcePuids         = "+sourcePuids+";");
					System.out.println("    destinationPuid     = "+CTO_Puid+";");
					System.out.println("    resultPuid          = "+PUID_String+";");
					System.out.println("    resultQty           = 1;");
					System.out.println("}");

					stringBuffer.append(rule_name+"\n");
					stringBuffer.append("    sourcePuids         = "+sourcePuids+";"+"\n");
					stringBuffer.append("    destinationPuid     = "+CTO_Puid+";"+"\n");
					stringBuffer.append("    resultPuid          = "+PUID_String+";"+"\n");
					stringBuffer.append("    resultQty           = 1;"+"\n");
					stringBuffer.append("}"+"\n");

				}

				if(two_conditions){
					rule_count+=2;
					String rule1_string= Rule_String.substring(0, Deriveon_end_index);
					String rule2_string= Rule_String.substring(Deriveon_end_index);
					String rule1_string_raw= Rule_String_raw.substring(0, Deriveon_end_index_raw);
					String rule2_string_raw= Rule_String_raw.substring(Deriveon_end_index_raw);					

					System.out.println("//"+PUID_String+"\t"+PN_String+"\t"+DESC_String);
					System.out.println("//"+Rule_String_raw);
					stringBuffer.append("//"+PUID_String+"\t"+PN_String+"\t"+DESC_String+"\n");
					stringBuffer.append("//"+rule1_string_raw);
					stringBuffer.append("//"+rule2_string_raw+"\n");
					List<String> list1 = extractMessage(rule1_string);
					DESC_String = DESC_String.replaceAll(" " , "");
					DESC_String = DESC_String.replaceAll("-" , "");

					String CTO_Puid="";
					String sourcePuids="";
					String CTO_PN_String_1="";
					for (int j = 0; j < list1.size(); j++) {
						String value= list1.get(j);
						if(value.contains("7Y")&&value.length()>5){
							CTO_Puid=CTO_FC_Map.get(value);
							CTO_PN_String_1=PN_String+"_"+value;
						}
						if(except_FCs.contains(value)) continue;
						if((!value.contains("Japan")&&(!value.contains("China")))){
							sourcePuids+="1x"+CTO_FC_Map.get(value)+",";
						}
					}
					String rule_name ="autoAdd "+CTO_PN_String_1+"_SVC_WW_1 extends ConditionalAdd {";
					sourcePuids=sourcePuids.substring(0,sourcePuids.length()-1);
					System.out.println(rule_name);
					System.out.println("    sourcePuids         = "+sourcePuids+";");
					System.out.println("    destinationPuid     = "+CTO_Puid+";");
					System.out.println("    resultPuid          = "+PUID_String+";");
					System.out.println("    resultQty           = 1;");
					System.out.println("}");
					stringBuffer.append(rule_name+"\n");
					stringBuffer.append("    sourcePuids         = "+sourcePuids+";"+"\n");
					stringBuffer.append("    destinationPuid     = "+CTO_Puid+";"+"\n");
					stringBuffer.append("    resultPuid          = "+PUID_String+";"+"\n");
					stringBuffer.append("    resultQty           = 1;"+"\n");
					stringBuffer.append("}"+"\n");

					List<String> list2 = extractMessage(rule2_string);
					DESC_String = DESC_String.replaceAll(" " , "");
					DESC_String = DESC_String.replaceAll("-" , "");

					String CTO_Puid2="";
					String sourcePuids2="";
					String CTO_PN_String_2="";
					for (int j = 0; j < list2.size(); j++) {
						String value= list2.get(j);
						if(value.contains("7Y")&&value.length()>5){
							CTO_Puid2=CTO_FC_Map.get(value);
							CTO_PN_String_2=PN_String+"_"+value;
						}
						if(except_FCs.contains(value)) continue;
						if((!value.contains("Japan")&&(!value.contains("China")))){
							sourcePuids2+="1x"+CTO_FC_Map.get(value)+",";
						}
					}
					String rule_name2 ="autoAdd "+CTO_PN_String_2+"_SVC_WW_2 extends ConditionalAdd {";
					sourcePuids2=sourcePuids2.substring(0,sourcePuids2.length()-1);
					System.out.println(rule_name2);
					System.out.println("    sourcePuids         = "+sourcePuids2+";");
					System.out.println("    destinationPuid     = "+CTO_Puid2+";");
					System.out.println("    resultPuid          = "+PUID_String+";");
					System.out.println("    resultQty           = 1;");
					System.out.println("}");
					stringBuffer.append(rule_name2+"\n");
					stringBuffer.append("    sourcePuids         = "+sourcePuids2+";"+"\n");
					stringBuffer.append("    destinationPuid     = "+CTO_Puid2+";"+"\n");
					stringBuffer.append("    resultPuid          = "+PUID_String+";"+"\n");
					stringBuffer.append("    resultQty           = 1;"+"\n");
					stringBuffer.append("}"+"\n");
				}


				//autoAdd DM3000H_Fondation_3Year_B3X4_SVC_WW extends ConditionalAdd { 
				//       sourcePuids         = 1x148802, 1x149881, 1x149871, 1x158853; 
				//       destinationPuid     = 148802; 
				//       resultPuid          = <PUID of 5WS7A18141>; 
				//       resultQty           = 1; 
				//}

			}
			//System.out.println("Software_FCs List Size: "+Software_FCs.size());
			System.out.println("count: " +count);
			System.out.println("rule_count: " +rule_count);
			saveDataToFile2(stringBuffer.toString());
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		return Duplicate_PNs;
	}
	public  ArrayList<String> extractMessage(String msg) {
		ArrayList<String> list = new ArrayList<String>();
		int start = 0;
		int startFlag = 0;
		int endFlag = 0;
		for (int i = 0; i < msg.length(); i++) {
			if (msg.charAt(i) == '(') {
				startFlag++;
				if (startFlag == endFlag + 1) {
					start = i;
				}
			} else if (msg.charAt(i) == ')') {
				endFlag++;
				if (endFlag == startFlag) {
					list.add(msg.substring(start + 1, i));
				}
			}
		}
		return list;
	}
	
	public static void saveDataToFile2 (String string) {
		try {
			File file = new File("D:/1_Service/Service.txt");

			if (!file.exists()) {
				file.createNewFile();
			}
			FileOutputStream outStream = new FileOutputStream(file);
			outStream.write(string.trim().getBytes());
			outStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	public Map<String,String> get_condition_FC_CTO_map_for_Service(String path){
		Map<String,String> Exist_condition_FC_CTO_map = new HashMap<>();
		XSSFWorkbook xssfWorkbook;
		try {
			xssfWorkbook = new XSSFWorkbook(path);

			XSSFSheet xssfSheet = xssfWorkbook.getSheet("FC_PUID");
			if(xssfSheet==null){
				xssfSheet = xssfWorkbook.getSheetAt(1);
			}
			if(xssfSheet==null){
				System.out.println("The Sheet is not Exist");
			}

			int eof_row= Common_Utils.getEOF_Row(xssfSheet, xssfSheet.getLastRowNum()+1);
			for(int i=1;i<eof_row;i++){
				XSSFRow row =xssfSheet.getRow(i);
				if(row==null) continue;
				XSSFCell FC_cell = row.getCell(0);
				String FC=FC_cell.toString();
				XSSFCell PUID_cell = row.getCell(1);
				String PUID=PUID_cell.toString();
				if(PUID.contains(".")){
					PUID =PUID.substring(0,PUID.lastIndexOf("."));
				}
				Exist_condition_FC_CTO_map.put(FC, PUID);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		return Exist_condition_FC_CTO_map;
	}
}
