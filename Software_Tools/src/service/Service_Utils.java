package service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import common.Common_Utils;
import product.Network;

public class Service_Utils {
	private String path;
	private String Update_Folder=Service_Main_View.Update_Folder;

	public Service_Utils(String path){
		this.path=path;
	}


	@SuppressWarnings("deprecation")
	public void Generate_SQL_creat_PUID_for_Service(String path){
		XSSFWorkbook xssfWorkbook;
		StringBuilder stringBuilder = new StringBuilder();
		try {
			xssfWorkbook = new XSSFWorkbook(path);

			XSSFSheet xssfSheet = xssfWorkbook.getSheet("Rules");
			if(xssfSheet==null){
				xssfSheet = xssfWorkbook.getSheetAt(0);
			}
			if(xssfSheet==null){
				System.out.println("The Sheet is not Exist");
			}

			ArrayList<String> PN_List= new ArrayList<>();

			int eof_row= Common_Utils.getEOF_Row(xssfSheet, xssfSheet.getLastRowNum()+1);
			for(int i=1;i<eof_row;i++){
				XSSFRow row =xssfSheet.getRow(i);
				if(row==null) continue;

				//check the PN whether have PUID
				XSSFCell puid_cell = row.getCell(0);
				if(puid_cell!=null){
					String PUID = puid_cell.toString();
					if(PUID.length()>1)  continue;
				}

				XSSFCell PN_cell = row.getCell(1);
				String PN=PN_cell.toString().trim();
				XSSFCell DESC_cell = row.getCell(2);
				String DESC=DESC_cell.toString();
				String MT="";
				String Model="";
				//if(PN.length()==7){
				MT=PN.substring(0,4);
				Model=PN.substring(4);
				//}
				//System.out.println(PN);
				PN_List.add(PN);
				String each_SQL="INSERT INTO DB2INST1.PRODUCT(DESCRIPTION,FT_UID,CAT_ID,MFG_ID,MT,OPT_PN,MODELX,VISIBLE,PRICED) values ('"+
						DESC+"\',25,421,1,\'"+MT+"\',\'"+PN+"\',\'"+Model+"\',1,\'Y\')\n";
				stringBuilder.append(each_SQL);
			}

			System.out.println("PN size: "+PN_List.size());
			String PN_LIST_String =List_To_String_Include_Signal(PN_List);
			String Select_SQL="select PUID,OPT_PN,DESCRIPTION from DB2INST1.PRODUCT where OPT_PN in("+PN_LIST_String.trim()+")";
			stringBuilder.append("\n\n");
			stringBuilder.append(Select_SQL+"\n");
			saveDataToFile3(stringBuilder.toString(),"D:/1_Service/Service_SQL_Create_PUID.txt");

		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	@SuppressWarnings("deprecation")
	public ArrayList<String> generate_CTO_FC_Item(String path){


		XSSFWorkbook xssfWorkbook;
		ArrayList<String> New_condition_FC_CTO_List =  new ArrayList<>();
		try {

			ArrayList<String> Exist_condition_FC_CTO_List = get_FC_CTO_List_From_Exist_map_for_Service(path);
			xssfWorkbook = new XSSFWorkbook(path);

			XSSFSheet xssfSheet = xssfWorkbook.getSheet("Rules");
			if(xssfSheet==null){
				xssfSheet = xssfWorkbook.getSheetAt(0);
			}
			if(xssfSheet==null){
				System.out.println("The Sheet is not Exist");
			}

			ArrayList<String>  ss = new ArrayList<>();
			int eof_row= Common_Utils.getEOF_Row(xssfSheet, xssfSheet.getLastRowNum()+1);

			for(int i=1;i<eof_row;i++){
				XSSFRow row =xssfSheet.getRow(i);
				if(row==null) continue;
				XSSFCell Content_cell = row.getCell(3);
				String Content=Content_cell.toString();
				//System.out.println(Content);
				Content = Content.replaceAll(" " , "");
				List<String> list = extractMessage(Content);
				for (int j = 0; j < list.size(); j++) {
					//System.out.println(i+"-->"+list.get(j));
					if(!ss.contains(list.get(j))){
						ss.add(list.get(j));
					}
				}
				//categoryIBMCognosAnalyticsAdministratorFilterItem extends FilterItem{
				//   	description		    =	"IBM Cognos Analytics Administrator";
				//}
			}
			//System.out.println("Software_FCs List Size: "+Software_FCs.size());


			ArrayList<String> MT_LIST = new ArrayList<>();
			ArrayList<String> FC_LIST = new ArrayList<>();

			System.out.println(ss.toString());
			for (int j = 0; j < ss.size(); j++) {
				System.out.println(ss.get(j));
				if(!Exist_condition_FC_CTO_List.contains(ss.get(j))){
					New_condition_FC_CTO_List.add(ss.get(j));
				}
				if(ss.get(j).contains("CTO")&&ss.get(j).length()>4){
					MT_LIST.add(ss.get(j).substring(0,4));
				}else{
					FC_LIST.add(ss.get(j));
				}
			}

			String MT_LIST_String =List_To_String_Include_Signal(MT_LIST);
			String FC_LIST_String =List_To_String_Include_Signal(FC_LIST);

			StringBuilder Sumary = new StringBuilder();
			for(int i=0;i<ss.size();i++){
				String FC=ss.get(i)+"\n";
				System.out.println(FC);
				Sumary.append(FC);
			}

			String SQL_search_FC_CTO="select PUID,FEAT_CODE,MT,MODELX from DB2INST1.PRODUCT  where FEAT_CODE in("+FC_LIST_String.trim()+") or MT in("+MT_LIST_String.trim()+")";
			Sumary.append(SQL_search_FC_CTO);

			if(New_condition_FC_CTO_List.size()>0){
				String Select_SQL="";
				for (String FC:New_condition_FC_CTO_List) {
					Select_SQL += "'"+FC.toString() + "',";
				}
				Select_SQL=Select_SQL.substring(0,Select_SQL.length()-1);
				Sumary.append("\n\n");
				Sumary.append("The SQL just for FC/CTO which need update to the source File:\n");
				Select_SQL="select FEAT_CODE,PUID from DB2INST1.PRODUCT  where FEAT_CODE in("+Select_SQL.trim()+")";
				Sumary.append("\n\n");
				Sumary.append(Select_SQL+"\n");
			}
			saveDataToFile3(Sumary.toString(),"D:/1_Service/Service_Condition_FC_CTO.txt");

		} catch (IOException e) {
			e.printStackTrace();
		}
		return New_condition_FC_CTO_List;
	}


	private String List_To_String_Include_Signal(ArrayList<String> Data_LIST) {

		String list_to_string="";
		for (String s:Data_LIST) {
			list_to_string += "'"+s+ "',";
		}
		list_to_string=list_to_string.substring(0,list_to_string.length()-1);
		return list_to_string;
	}

	@SuppressWarnings("deprecation")
	public ArrayList<String> get_FC_CTO_List_From_Exist_map_for_Service(String path){
		ArrayList<String> Exist_condition_FC_CTO_List = new ArrayList<>();
		XSSFWorkbook xssfWorkbook;
		try {
			xssfWorkbook = new XSSFWorkbook(path);

			XSSFSheet xssfSheet = xssfWorkbook.getSheet("FC_PUID");
			if(xssfSheet==null){
				xssfSheet = xssfWorkbook.getSheetAt(1);
			}
			if(xssfSheet==null){
				System.out.println("The Sheet is not Exist");
			}

			int eof_row= Common_Utils.getEOF_Row(xssfSheet, xssfSheet.getLastRowNum()+1);
			for(int i=1;i<eof_row;i++){
				XSSFRow row =xssfSheet.getRow(i);
				if(row==null) continue;
				XSSFCell FC_cell = row.getCell(0);
				String FC=FC_cell.toString();
				Exist_condition_FC_CTO_List.add(FC);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		return Exist_condition_FC_CTO_List;
	}


	public Map<String,String> get_condition_FC_CTO_map_for_Service(String path){
		Map<String,String> Exist_condition_FC_CTO_map = new HashMap<>();
		XSSFWorkbook xssfWorkbook;
		try {
			xssfWorkbook = new XSSFWorkbook(path);

			XSSFSheet xssfSheet = xssfWorkbook.getSheet("FC_PUID");
			if(xssfSheet==null){
				xssfSheet = xssfWorkbook.getSheetAt(1);
			}
			if(xssfSheet==null){
				System.out.println("The Sheet is not Exist");
			}

			int eof_row= Common_Utils.getEOF_Row(xssfSheet, xssfSheet.getLastRowNum()+1);
			for(int i=1;i<eof_row;i++){
				XSSFRow row =xssfSheet.getRow(i);
				if(row==null) continue;
				XSSFCell FC_cell = row.getCell(0);
				String FC=FC_cell.toString();
				XSSFCell PUID_cell = row.getCell(1);
				String PUID=PUID_cell.toString();
				if(PUID.contains(".")){
					PUID =PUID.substring(0,PUID.lastIndexOf("."));
				}
				Exist_condition_FC_CTO_map.put(FC, PUID);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		return Exist_condition_FC_CTO_map;
	}
	@SuppressWarnings("deprecation")
	public void set_map_for_Service(String path){
		XSSFWorkbook xssfWorkbook;
		try {
			xssfWorkbook = new XSSFWorkbook(path);

			XSSFSheet xssfSheet = xssfWorkbook.getSheet("FC_PUID");
			if(xssfSheet==null){
				xssfSheet = xssfWorkbook.getSheetAt(0);
			}
			if(xssfSheet==null){
				System.out.println("The Sheet is not Exist");
			}
			StringBuffer stringBuffer = new StringBuffer();
			int eof_row= Common_Utils.getEOF_Row(xssfSheet, xssfSheet.getLastRowNum()+1);
			for(int i=1;i<eof_row;i++){
				XSSFRow row =xssfSheet.getRow(i);
				if(row==null) continue;
				XSSFCell FC_cell = row.getCell(0);
				String FC=FC_cell.toString();
				XSSFCell PUID_cell = row.getCell(1);
				String PUID=PUID_cell.toString();
				if(PUID.contains(".")){
					System.out.println("kkkkkkkkk");
					PUID=PUID.substring(0,PUID.lastIndexOf("."));
				}
				//CTO_FC_Map.put("7Y42CTO1WW","148802");
				System.out.println("CTO_FC_Map.put(\""+FC+"\",\""+PUID+"\");\n");
				stringBuffer.append("CTO_FC_Map.put(\""+FC+"\",\""+PUID+"\");\n");
			}
			stringBuffer.append("\n");
			//"C:/Users/xiexg1/Desktop/Service/Service_Search_PUID_2.9.xlsx"
			saveDataToFile3(stringBuffer.toString(),"C:/Users/xiexg1/Desktop/Service/Service_Source_FC_PUID.txt");
			//System.out.println("Software_FCs List Size: "+Software_FCs.size());


		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	static String AddDateForFileName(String s) {
		String s1 = s.substring(0,s.lastIndexOf("."));
		String s2 = s.substring(s.lastIndexOf(".")+1);
		Calendar cale = Calendar.getInstance();  
		int year = cale.get(Calendar.YEAR);  
		int month = cale.get(Calendar.MONTH) + 1;  
		int day = cale.get(Calendar.DATE);  
		if (month<10) {
			return s1+"_"+year+"0"+month+day+"."+s2;
		}else{
			return s1+"_"+year+month+day+"."+s2;
		}
	}
	@SuppressWarnings("deprecation")
	public String get_PUID_DB_for_Source_File(String path){
		XSSFWorkbook xssfWorkbook;

		String Folder = path.substring(0,path.lastIndexOf("\\"));
		String Filename = path.substring(path.lastIndexOf("\\")+1);
		Filename=AddDateForFileName(Filename);
		String output_path = Folder+"\\"+Update_Folder+"\\"+Filename;


		File out_folder = new File(Folder+"\\"+Update_Folder);
		if(!out_folder.exists()){
			out_folder.mkdir();
		}
		//System.out.println(Folder);
		//System.out.println(Filename);
		//System.out.println(output_path);
		try {
			FileOutputStream fout = new FileOutputStream(output_path);
			xssfWorkbook = new XSSFWorkbook(path);

			XSSFSheet xssfSheet = xssfWorkbook.getSheet("Services");
			if(xssfSheet==null){
				xssfSheet = xssfWorkbook.getSheetAt(0);
			}
			if(xssfSheet==null){
				System.out.println("The Sheet is not Exist");
			}

			String content="";
			int eof_row= Common_Utils.getEOF_Row(xssfSheet, xssfSheet.getLastRowNum()+1);
			for(int i=1;i<eof_row;i++){
				XSSFRow row =xssfSheet.getRow(i);
				if(row==null) continue;
				XSSFCell FC_cell = row.getCell(1);
				String FC=FC_cell.toString();
				content += "'"+FC + "',";
			}
			content=content.substring(0,content.length()-1);
			String sql="select PUID,OPT_PN from DB2INST1.PRODUCT  where OPT_PN in("+content.trim()+")";
			System.out.println("select PUID,OPT_PN from DB2INST1.PRODUCT  where OPT_PN in("+content.trim()+")");

			Map<String,String> maps = query_FC_Under_FeMale(sql);
			System.out.println("maps.size: "+maps.size());
			Thread.sleep(1000);
			for(int i=1;i<eof_row;i++){
				XSSFRow row =xssfSheet.getRow(i);
				if(row==null) continue;
				XSSFCell FC_cell = row.getCell(1);
				String FC=FC_cell.toString();
				XSSFCell cell=row.createCell(0);
				System.out.println(maps.get(FC));
				cell.setCellValue(maps.get(FC));
			}
			fout.flush();
			xssfWorkbook.write(fout);
			fout.close();

		} catch (Exception e) {
			e.printStackTrace();
		} 
		return output_path;
	}

	private Map<String, String> query_FC_Under_FeMale(String sql) throws SQLException {

		Map<String,String> maps = new HashMap<>();
		Network.connectDatabase();
		ResultSet FC_rs_Under_FeMale=Network.stmt.executeQuery(sql);

		while(FC_rs_Under_FeMale.next())
		{
			//System.out.println(FC_rs_Under_FeMale.getString(2).toString());
			maps.put(FC_rs_Under_FeMale.getString(2).toString(), FC_rs_Under_FeMale.getString(1).toString());
		}
		FC_rs_Under_FeMale.close();

		return maps;	
	}

	@SuppressWarnings("deprecation")
	public List<String>  generate_AML_Code_for_Service(String path){
		XSSFWorkbook xssfWorkbook;
		List<String> ALL_PNs= new ArrayList<String>();
		List<String> Duplicate_PNs= new ArrayList<String>();


		//String dist_file_path = Folder_path+"\\Update\\"+filename;
		//System.out.println("path: "+source_file_path);
		try {
			xssfWorkbook = new XSSFWorkbook(path);

			XSSFSheet xssfSheet = xssfWorkbook.getSheet("Services");
			if(xssfSheet==null){
				xssfSheet = xssfWorkbook.getSheetAt(0);
			}
			if(xssfSheet==null){
				System.out.println("The Sheet is not Exist");
			}

			//ArrayList<String>  ss = new ArrayList<>();
			int eof_row= Common_Utils.getEOF_Row(xssfSheet, xssfSheet.getLastRowNum()+1);
			//Map<String, String>  CTO_FC_Map = get_map();
			Map<String, String>  CTO_FC_Map = get_condition_FC_CTO_map_for_Service(path);
			int count=0;
			int rule_count=0;
			StringBuffer stringBuffer = new StringBuffer();
			String except_FCs="B470,B471,B472,B473,B474,-Japan,-China";
			for(int i=1;i<eof_row;i++){
				XSSFRow row =xssfSheet.getRow(i);
				if(row==null) continue;
				XSSFCell PUID_cell = row.getCell(0);
				if(PUID_cell==null) continue;
				String PUID_String=PUID_cell.toString();
				if(PUID_String.length() < 1) continue;
				XSSFCell PN_cell = row.getCell(1);
				String PN_String=PN_cell.toString();
				XSSFCell DESC_cell = row.getCell(2);
				String DESC_String=DESC_cell.toString();
				XSSFCell Rule_cell = row.getCell(3);
				String Rule_String_raw=Rule_cell.toString();
				String Rule_String = Rule_String_raw.replaceAll(" " , "");

				//System.out.println(Rule_String);

				//int Deriveon_start_index_raw=Rule_String_raw.indexOf("Derive");
				int Deriveon_end_index_raw=Rule_String_raw.lastIndexOf("Derive");

				int Deriveon_start_index=Rule_String.indexOf("Deriveon");
				int Deriveon_end_index=Rule_String.lastIndexOf("Deriveon");

				boolean two_conditions= false;
				if(Deriveon_start_index!=Deriveon_end_index){
					//System.out.println(PN_String+":  "+Rule_String);
					//					String rule1_string= Rule_String.substring(0, Deriveon_end_index);
					//					String rule2_string= Rule_String.substring(Deriveon_end_index);
					//					System.out.println("rule1_string: "+rule1_string);
					//					System.out.println("rule2_string: "+rule2_string);
					two_conditions= true;
					count++;
				}

				if(!ALL_PNs.contains(PN_String)){
					ALL_PNs.add(PN_String);
				}else{
					Duplicate_PNs.add(PN_String);
				}

				if(!two_conditions){
					rule_count++;
					List<String> list = extractMessage(Rule_String);
					//System.out.println("//"+PUID_String+"\t"+PN_String+"\t"+DESC_String);
					//System.out.println("//"+Rule_String_raw);
					stringBuffer.append("//"+PUID_String+"\t"+PN_String+"\t"+DESC_String+"\n");
					stringBuffer.append("//"+Rule_String_raw+"\n");


					DESC_String = DESC_String.replaceAll(" " , "");
					DESC_String = DESC_String.replaceAll("-" , "");
					String CTO_PN_String="";
					String CTO_Puid="";
					String sourcePuids="";
					for (int j = 0; j < list.size(); j++) {
						String value= list.get(j);
						if(value.contains("7Y")&&value.length()>5){
							CTO_Puid=CTO_FC_Map.get(value);
							CTO_PN_String=PN_String+"_"+value;
						}
						if(except_FCs.contains(value)) continue;
						if((!value.contains("Japan")&&(!value.contains("China")))){
							sourcePuids+="1x"+CTO_FC_Map.get(value)+",";
						}
					}
					if(sourcePuids.length()<2)
					{
						System.out.println(PUID_String+" "+PN_String);
					}
					String rule_name ="autoAdd "+CTO_PN_String+"_SVC_WW extends ConditionalAdd {";
					sourcePuids=sourcePuids.substring(0,sourcePuids.length()-1);
					System.out.println(rule_name);
					System.out.println("    sourcePuids         = "+sourcePuids+";");
					System.out.println("    destinationPuid     = "+CTO_Puid+";");
					System.out.println("    resultPuid          = "+PUID_String+";");
					System.out.println("    resultQty           = 1;");
					System.out.println("}");

					stringBuffer.append(rule_name+"\n");
					stringBuffer.append("    sourcePuids         = "+sourcePuids+";"+"\n");
					stringBuffer.append("    destinationPuid     = "+CTO_Puid+";"+"\n");
					stringBuffer.append("    resultPuid          = "+PUID_String+";"+"\n");
					stringBuffer.append("    resultQty           = 1;"+"\n");
					stringBuffer.append("}"+"\n");

				}

				if(two_conditions){
					rule_count+=2;
					String rule1_string= Rule_String.substring(0, Deriveon_end_index);
					String rule2_string= Rule_String.substring(Deriveon_end_index);
					String rule1_string_raw= Rule_String_raw.substring(0, Deriveon_end_index_raw);
					String rule2_string_raw= Rule_String_raw.substring(Deriveon_end_index_raw);					

					System.out.println("//"+PUID_String+"\t"+PN_String+"\t"+DESC_String);
					System.out.println("//"+Rule_String_raw);
					stringBuffer.append("//"+PUID_String+"\t"+PN_String+"\t"+DESC_String+"\n");
					stringBuffer.append("//"+rule1_string_raw);
					stringBuffer.append("//"+rule2_string_raw+"\n");
					List<String> list1 = extractMessage(rule1_string);
					DESC_String = DESC_String.replaceAll(" " , "");
					DESC_String = DESC_String.replaceAll("-" , "");

					String CTO_Puid="";
					String sourcePuids="";
					String CTO_PN_String_1="";
					for (int j = 0; j < list1.size(); j++) {
						String value= list1.get(j);
						if(value.contains("7Y")&&value.length()>5){
							CTO_Puid=CTO_FC_Map.get(value);
							CTO_PN_String_1=PN_String+"_"+value;
						}
						if(except_FCs.contains(value)) continue;
						if((!value.contains("Japan")&&(!value.contains("China")))){
							sourcePuids+="1x"+CTO_FC_Map.get(value)+",";
						}
					}
					String rule_name ="autoAdd "+CTO_PN_String_1+"_SVC_WW_1 extends ConditionalAdd {";
					sourcePuids=sourcePuids.substring(0,sourcePuids.length()-1);
					System.out.println(rule_name);
					System.out.println("    sourcePuids         = "+sourcePuids+";");
					System.out.println("    destinationPuid     = "+CTO_Puid+";");
					System.out.println("    resultPuid          = "+PUID_String+";");
					System.out.println("    resultQty           = 1;");
					System.out.println("}");
					stringBuffer.append(rule_name+"\n");
					stringBuffer.append("    sourcePuids         = "+sourcePuids+";"+"\n");
					stringBuffer.append("    destinationPuid     = "+CTO_Puid+";"+"\n");
					stringBuffer.append("    resultPuid          = "+PUID_String+";"+"\n");
					stringBuffer.append("    resultQty           = 1;"+"\n");
					stringBuffer.append("}"+"\n");

					List<String> list2 = extractMessage(rule2_string);
					DESC_String = DESC_String.replaceAll(" " , "");
					DESC_String = DESC_String.replaceAll("-" , "");

					String CTO_Puid2="";
					String sourcePuids2="";
					String CTO_PN_String_2="";
					for (int j = 0; j < list2.size(); j++) {
						String value= list2.get(j);
						if(value.contains("7Y")&&value.length()>5){
							CTO_Puid2=CTO_FC_Map.get(value);
							CTO_PN_String_2=PN_String+"_"+value;
						}
						if(except_FCs.contains(value)) continue;
						if((!value.contains("Japan")&&(!value.contains("China")))){
							sourcePuids2+="1x"+CTO_FC_Map.get(value)+",";
						}
					}
					String rule_name2 ="autoAdd "+CTO_PN_String_2+"_SVC_WW_2 extends ConditionalAdd {";
					sourcePuids2=sourcePuids2.substring(0,sourcePuids2.length()-1);
					System.out.println(rule_name2);
					System.out.println("    sourcePuids         = "+sourcePuids2+";");
					System.out.println("    destinationPuid     = "+CTO_Puid2+";");
					System.out.println("    resultPuid          = "+PUID_String+";");
					System.out.println("    resultQty           = 1;");
					System.out.println("}");
					stringBuffer.append(rule_name2+"\n");
					stringBuffer.append("    sourcePuids         = "+sourcePuids2+";"+"\n");
					stringBuffer.append("    destinationPuid     = "+CTO_Puid2+";"+"\n");
					stringBuffer.append("    resultPuid          = "+PUID_String+";"+"\n");
					stringBuffer.append("    resultQty           = 1;"+"\n");
					stringBuffer.append("}"+"\n");
				}


				//autoAdd DM3000H_Fondation_3Year_B3X4_SVC_WW extends ConditionalAdd { 
				//       sourcePuids         = 1x148802, 1x149881, 1x149871, 1x158853; 
				//       destinationPuid     = 148802; 
				//       resultPuid          = <PUID of 5WS7A18141>; 
				//       resultQty           = 1; 
				//}

			}
			//System.out.println("Software_FCs List Size: "+Software_FCs.size());
			System.out.println("count: " +count);
			System.out.println("rule_count: " +rule_count);
			saveDataToFile2(stringBuffer.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}

		return Duplicate_PNs;
	}
	public  List<String> extractMessage(String msg) {
		List<String> list = new ArrayList<String>();
		int start = 0;
		int startFlag = 0;
		int endFlag = 0;
		for (int i = 0; i < msg.length(); i++) {
			if (msg.charAt(i) == '(') {
				startFlag++;
				if (startFlag == endFlag + 1) {
					start = i;
				}
			} else if (msg.charAt(i) == ')') {
				endFlag++;
				if (endFlag == startFlag) {
					list.add(msg.substring(start + 1, i));
				}
			}
		}
		return list;
	}
	public Map<String,String> get_map(){
		Map<String, String>  CTO_FC_Map = new HashMap<String,String>();
		CTO_FC_Map.put("7Y40CTO1WW","148800");
		CTO_FC_Map.put("7Y41CTO1WW","148801");
		CTO_FC_Map.put("7Y42CTO1WW","148802");
		CTO_FC_Map.put("7Y43CTO1WW","148803");
		CTO_FC_Map.put("7Y56CTO1WW","148865");
		CTO_FC_Map.put("7Y57CTO1WW","148828");
		CTO_FC_Map.put("7Y58CTO1WW","148862");
		CTO_FC_Map.put("7Y59CTO1WW","148866");
		CTO_FC_Map.put("B0W1","149871");
		CTO_FC_Map.put("B0W2","149872");
		CTO_FC_Map.put("B3W2","148847");
		CTO_FC_Map.put("B3W3","148838");
		CTO_FC_Map.put("B3W5","148870");
		CTO_FC_Map.put("B3W6","148871");
		CTO_FC_Map.put("B3W7","148848");
		CTO_FC_Map.put("B3W8","148839");
		CTO_FC_Map.put("B3WC","148875");
		CTO_FC_Map.put("B3WD","148876");
		CTO_FC_Map.put("B3WE","148849");
		CTO_FC_Map.put("B3WF","148840");
		CTO_FC_Map.put("B3WJ","148879");
		CTO_FC_Map.put("B3WK","148850");
		CTO_FC_Map.put("B3WL","148841");
		CTO_FC_Map.put("B3WM","148880");
		CTO_FC_Map.put("B3WN","148881");
		CTO_FC_Map.put("B3WP","148882");
		CTO_FC_Map.put("B3WQ","148851");
		CTO_FC_Map.put("B3WR","148842");
		CTO_FC_Map.put("B3WS","148883");
		CTO_FC_Map.put("B3WT","148884");
		CTO_FC_Map.put("B3WU","148885");
		CTO_FC_Map.put("B3WV","148852");
		CTO_FC_Map.put("B3WW","148843");
		CTO_FC_Map.put("B3WX","148886");
		CTO_FC_Map.put("B3WY","148844");
		CTO_FC_Map.put("B3WZ","148887");
		CTO_FC_Map.put("B3X0","148845");
		CTO_FC_Map.put("B3X1","148888");
		CTO_FC_Map.put("B3X2","148846");
		CTO_FC_Map.put("B3X3","148889");
		CTO_FC_Map.put("B3X4","148853");
		CTO_FC_Map.put("B3X5","148897");
		CTO_FC_Map.put("B3X6","148854");
		CTO_FC_Map.put("B3X7","148898");
		CTO_FC_Map.put("B3X8","148855");
		CTO_FC_Map.put("B3X9","148899");
		CTO_FC_Map.put("B3XA","148856");
		CTO_FC_Map.put("B3XB","148857");
		CTO_FC_Map.put("B3XC","148858");
		CTO_FC_Map.put("B3XD","148859");
		CTO_FC_Map.put("B3XE","148860");
		CTO_FC_Map.put("B3XF","148861");
		CTO_FC_Map.put("B46X","149880");
		CTO_FC_Map.put("B46Y","149881");
		CTO_FC_Map.put("B470","149866");
		CTO_FC_Map.put("B471","149867");
		CTO_FC_Map.put("B472","149868");
		CTO_FC_Map.put("B473","149869");
		CTO_FC_Map.put("B474","149889");
		CTO_FC_Map.put("B48J","149882");
		CTO_FC_Map.put("B48K","149884");	
		CTO_FC_Map.put("B2UW","150972");

		CTO_FC_Map.put("B5QE","158686");
		CTO_FC_Map.put("B5QF","158687");
		CTO_FC_Map.put("B5QG","158688");
		CTO_FC_Map.put("B5QH","158689");
		CTO_FC_Map.put("B5QJ","158690");
		CTO_FC_Map.put("B5QK","158691");
		CTO_FC_Map.put("B5QL","158692");
		CTO_FC_Map.put("B5QM","158693");
		CTO_FC_Map.put("B5QN","158694");
		CTO_FC_Map.put("B5RH","159839");
		CTO_FC_Map.put("B5Q8","159841");
		CTO_FC_Map.put("B5Q9","159842");
		CTO_FC_Map.put("B5QA","159843");
		CTO_FC_Map.put("B5QB","159844");
		CTO_FC_Map.put("B5QC","159845");
		CTO_FC_Map.put("B5QD","159846");
		CTO_FC_Map.put("B5QP","159847");
		CTO_FC_Map.put("B5QQ","159848");
		CTO_FC_Map.put("B5QR","159849");
		CTO_FC_Map.put("B5R8","159850");
		CTO_FC_Map.put("B5R9","159851");
		CTO_FC_Map.put("B5RA","159852");
		CTO_FC_Map.put("B5RB","159853");
		CTO_FC_Map.put("B5RC","159854");
		CTO_FC_Map.put("B5RD","159855");
		CTO_FC_Map.put("B5RE","159857");
		CTO_FC_Map.put("B5RF","159858");
		CTO_FC_Map.put("B5RG","159859");
		CTO_FC_Map.put("B5QS","159861");
		CTO_FC_Map.put("B5QT","159862");
		CTO_FC_Map.put("B5QU","159863");
		CTO_FC_Map.put("B5QV","159864");
		CTO_FC_Map.put("B5QW","159865");
		CTO_FC_Map.put("B5QX","159866");
		CTO_FC_Map.put("B5QY","159867");
		CTO_FC_Map.put("B5QZ","159868");
		CTO_FC_Map.put("B5R0","159869");
		CTO_FC_Map.put("B5R1","159870");
		CTO_FC_Map.put("B5R2","160214");
		CTO_FC_Map.put("B5R4","160216");
		CTO_FC_Map.put("B5R5","160217");
		CTO_FC_Map.put("B5R6","160219");
		CTO_FC_Map.put("B5R7","160221");
		return CTO_FC_Map;
	}
	public void saveDataToFile (String string) {

		try {
			//			String Folder_path=path.substring(0, path.lastIndexOf("\\"));
			//			String out_put_path_folder = Folder_path+"\\"+Update_String;
			//			File out_folder = new File(out_put_path_folder);
			//			if(!out_folder.exists()){
			//				out_folder.mkdir();
			//			}
			File file = new File("D:/1_SW/Software_SQL_Create_PUID.txt");
			if (!file.exists()) {
				file.createNewFile();
			}
			FileOutputStream outStream = new FileOutputStream(file);
			outStream.write(string.trim().getBytes());
			outStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static void saveDataToFile2 (String string) {
		try {

			//SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
			//String formatted = format1.format(new Date());
			Calendar cale = Calendar.getInstance();  
			int month = cale.get(Calendar.MONTH) + 1;  
			String filepath="";
			if(month<10){
				filepath="D:/1_Service/Service_"+cale.get(Calendar.YEAR)+"0"+month+cale.get(Calendar.DATE)+".txt";
			}else{
				filepath="D:/1_Service/Service_"+cale.get(Calendar.YEAR)+month+cale.get(Calendar.DATE)+".txt";
			}
			File file = new File(filepath);

			if (!file.exists()) {
				file.createNewFile();
			}
			FileOutputStream outStream = new FileOutputStream(file);
			outStream.write(string.trim().getBytes());
			outStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static void saveDataToFile3 (String string,String Path) {
		try {
			//File file = new File("D:/CombineFC2.txt");
			File file = new File(Path);
			if (!file.exists()) {
				file.createNewFile();
			}
			FileOutputStream outStream = new FileOutputStream(file);
			outStream.write(string.trim().getBytes());
			outStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


}
