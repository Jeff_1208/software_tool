package product;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import common.Common_Utils;
import entity.Software_FC;
import software.Software_Main_View;

public class Product_SQL {

	private String SQL_Start="INSERT INTO DB2INST1.PRODUCT(DESCRIPTION,FT_UID,CAT_ID,MFG_ID,BID,FEAT_CODE,OPT_PN,VISIBLE,PASSTHRU,PRICED) values ('";
	//',	16	,	361	,	1	,	1	,	'	B2HK	',	'	7S000025WW	',	1	,	0	,	'	Y	'	);
	private String SQL_Parameter_1="\',16,361,1,1,'";
	private String SQL_Parameter_2="\',1,0,\'Y\')";
	private List<Software_FC>  Software_FCs= new ArrayList<>();

	private int Loadsheet_FC_Column=0;
	private int Loadsheet_PN_Column=0;
	private int Loadsheet_DESC_Column=0;

	private String path;
	private String Update_String=Software_Main_View.Update_Folder;

	public Product_SQL(String path){
		this.path=path;
	}
	public static int get_FC_Column_from_Loadsheet(XSSFSheet xssfSheet,String flag_String) {
		int Flag_Column=0;
		XSSFRow row =xssfSheet.getRow(0);
		for(int i=0;i<row.getLastCellNum();i++){
			XSSFCell cell = row.getCell(i);
			if(cell==null) continue;
			String s=cell.toString();
			if(s.equals(flag_String)||s.contains(flag_String)){
				Flag_Column=i;
				System.out.println("get "+flag_String+" Column: "+i);
				return Flag_Column;
			}
		}
		if(flag_String.contains("PN")&&Flag_Column==0){
			Flag_Column=1;
		}
		if(flag_String.contains("Description")&&Flag_Column==0){
			Flag_Column=2;
		}
		return Flag_Column;
	}

	public static int get_FC_Column_from_Loadsheet_Public(Sheet xssfSheet,String flag_String) {
		int Flag_Column=0;
		Row row =xssfSheet.getRow(0);
		for(int i=0;i<row.getLastCellNum();i++){
			Cell cell = row.getCell(i);
			if(cell==null) continue;
			String s=cell.toString();
			if(s.equals(flag_String)||s.contains(flag_String)){
				Flag_Column=i;
				System.out.println("get "+flag_String+" Column: "+i);
				return Flag_Column;
			}
		}
		if(flag_String.contains("PN")&&Flag_Column==0){
			Flag_Column=1;
		}
		if(flag_String.contains("Description")&&Flag_Column==0){
			Flag_Column=2;
		}
		return Flag_Column;
	}

	public static int get_FC_Column_from_Loadsheet_2003(HSSFSheet hssfSheet,String flag_String) {
		int Flag_Column=0;
		HSSFRow row =hssfSheet.getRow(0);
		for(int i=0;i<row.getLastCellNum();i++){
			HSSFCell cell = row.getCell(i);
			if(cell==null) continue;
			String s=cell.toString();
			if(s.equals(flag_String)||s.contains(flag_String)){
				Flag_Column=i;
				System.out.println("get "+flag_String+" Column: "+i);
				return Flag_Column;
			}
		}
		return Flag_Column;
	}


	@SuppressWarnings("deprecation")
	public void generate_SQL_Update_Description(String path){
		XSSFWorkbook xssfWorkbook;
		try {
			xssfWorkbook = new XSSFWorkbook(path);

			XSSFSheet xssfSheet = xssfWorkbook.getSheet("Pivot3 Mapping");
			if(xssfSheet==null){
				xssfSheet = xssfWorkbook.getSheetAt(0);
			}
			if(xssfSheet==null){
				System.out.println("The Sheet is not Exist");
			}
			List<String>  DESC_FCs= new ArrayList<>();
			List<String>  PNs= new ArrayList<>();
			StringBuffer stringBuffer = new StringBuffer();
			int eof_row= Common_Utils.getEOF_Row(xssfSheet, xssfSheet.getLastRowNum()+1);
			for(int i=1;i<eof_row;i++){
				XSSFRow row =xssfSheet.getRow(i);
				if(row==null) continue;
				XSSFCell FC_cell = row.getCell(0);
				String FC=FC_cell.toString();
				DESC_FCs.add(FC);
				XSSFCell PN_cell = row.getCell(1);
				String PN=PN_cell.toString();
				if(!PN.contains("N/A")){
					PNs.add(PN);
				}
				XSSFCell desc_cell = row.getCell(2);
				String DESC=desc_cell.toString();
				String s ="update DB2INST1.PRODUCT set DESCRIPTION='"+DESC+"' where FEAT_CODE ='"+FC+"' and CAT_ID = '361'\n";
				System.out.println(s);
				stringBuffer.append(s);
				if(!PN.contains("N/A")){
					String s1 ="update DB2INST1.PRODUCT set DESCRIPTION='"+DESC+"' where OPT_PN ='"+PN+"' and CAT_ID = '361'\n";
					System.out.println(s1);
					stringBuffer.append(s1);
				}
				//String s ="CTO_FC_Map.put(\""+FC+"\",\""+DESC+"\");";
				//CTO_FC_Map.put("FC", "PUID");	

			}
			String Select_SQL="";
			for (String FC:DESC_FCs) {
				Select_SQL += "'"+FC + "',";
			}
			Select_SQL=Select_SQL.substring(0,Select_SQL.length()-1);
			String Select_SQL_2="select PUID,FEAT_CODE,DESCRIPTION,OPT_PN from DB2INST1.PRODUCT  where FEAT_CODE in("+Select_SQL.trim()+")";
			stringBuffer.append("\n");
			stringBuffer.append(Select_SQL_2);
			if(PNs.size()>0){
				String PN_String="";
				for (String PN:PNs) {
					PN_String += "'"+PN + "',";
				}
				PN_String=PN_String.substring(0,PN_String.length()-1);
				String Select_SQL_3="select PUID,FEAT_CODE,DESCRIPTION,OPT_PN from DB2INST1.PRODUCT  where OPT_PN in("+PN_String.trim()+")";
				stringBuffer.append("\n");
				stringBuffer.append(Select_SQL_3);
			}

			saveDataToFile3(stringBuffer.toString(),"D:/1_SW/SQL_For_SW_Update_Description.txt");
			//System.out.println("Software_FCs List Size: "+Software_FCs.size());


		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public String Get_String(String s){
		Pattern pattern = Pattern.compile("[a-zA-Z0-9]");
		int index =0;
		if(s.substring(0,1).equals("O")){
			s= s.substring(1);
		}
		for(int i=0;i<s.length();i++){
			String t= s.substring(i, i+1);
			if(pattern.matcher(t).matches()){
				index=i;
				//System.out.println("index: "+index);
				break;
			}
		}
		return s.substring(index);
	}
	public String Get_No_empty_String(String s){
		Pattern pattern = Pattern.compile("^[_a-zA-Z0-9]");
		char[] c = s.toCharArray();
		ArrayList<String> list = new ArrayList<>();
		for(int i=0;i<c.length;i++){
			String t= c[i]+"";
			if(pattern.matcher(t).matches()){
				list.add(t);
			}
		}
		String no_empty_String = list.toString();
		no_empty_String=no_empty_String.replaceAll(",", "");
		no_empty_String=no_empty_String.replaceAll(" ", "");
		no_empty_String=no_empty_String.substring(1, no_empty_String.length()-1);
		//System.out.println(no_empty_String);
		return no_empty_String;
	}
	@SuppressWarnings("deprecation")
	public void generate_Category_Filter_Item(String path){
		XSSFWorkbook xssfWorkbook;
		try {
			xssfWorkbook = new XSSFWorkbook(path);

			XSSFSheet xssfSheet = xssfWorkbook.getSheet("Pivot3 Mapping");
			if(xssfSheet==null){
				xssfSheet = xssfWorkbook.getSheetAt(0);
			}
			if(xssfSheet==null){
				System.out.println("The Sheet is not Exist");
			}
			//英文字母:[a-zA-Z]
			//匹配英文字母和数字及下划线：^[_a-zA-Z0-9]+$
			ArrayList<String>  ss = new ArrayList<>();
			int eof_row= Common_Utils.getEOF_Row(xssfSheet, xssfSheet.getLastRowNum()+1);
			StringBuffer stringBuffer = new StringBuffer();
			for(int i=1;i<eof_row;i++){
				XSSFRow row =xssfSheet.getRow(i);
				if(row==null) continue;
				XSSFCell category_cell = row.getCell(0);
				String category=category_cell.toString().trim();
				if(category.substring(0,1).equals("O")){
					category= category.substring(1);
				}
				category= Get_String(category);
				String category_no_empty_string = Get_No_empty_String(category);
				System.out.println("//"+category);
				System.out.println("category "+ category_no_empty_string+"FilterItem extends FilterItem{");
				System.out.println("     description    =   \""+category+"\";");
				System.out.println("}");
				stringBuffer.append("//"+category+"\n");
				stringBuffer.append("category "+ category_no_empty_string+"FilterItem extends FilterItem{\n");
				stringBuffer.append("     description    =   \""+category+"\";\n");
				stringBuffer.append("}\n");
				String s = category_no_empty_string+"FilterItem";
				ss.add(s);
				//categoryIBMCognosAnalyticsAdministratorFilterItem extends FilterItem{
				//   	description		    =	"IBM Cognos Analytics Administrator";
				//}
			}
			//System.out.println("Software_FCs List Size: "+Software_FCs.size());
			System.out.println(ss.toString());
			stringBuffer.append("\n"+ss.toString()+"\n\n");
			for(int i=0;i<ss.size();i++){
				System.out.println(ss.get(i));
				stringBuffer.append(ss.get(i)+"\n");
			}

			saveDataToFile3(stringBuffer.toString(),"D:/1_SW/Generate_FilterItem.txt");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("deprecation")
	public void generate_Category_List_Filter_Item(String path){
		XSSFWorkbook xssfWorkbook;
		try {
			StringBuffer stringBuffer = new StringBuffer();
			xssfWorkbook = new XSSFWorkbook(path);

			XSSFSheet xssfSheet = xssfWorkbook.getSheet("FilterItem");
			if(xssfSheet==null){
				xssfSheet = xssfWorkbook.getSheetAt(1);
			}
			if(xssfSheet==null){
				System.out.println("The Sheet is not Exist");
			}
			//英文字母:[a-zA-Z]
			//匹配英文字母和数字及下划线：^[_a-zA-Z0-9]+$

			//     //*****************定义Category 和Class***************//
			//	   //*****************定义Category 和Class***************//
			int eof_column= Common_Utils.getEOF_Column(xssfSheet,0);
			//System.out.println("eof_column: "+eof_column);
			ArrayList<String>  FilterList = new ArrayList<>();
			for(int i=1;i<eof_column;i++){


				XSSFRow row =xssfSheet.getRow(0);
				if(row==null) break;
				XSSFCell category_cell = row.getCell(i);
				if(category_cell==null) break;
				String category=category_cell.toString().trim();
				if(category.length()<2) break;

				category= Get_String(category);
				String category_no_empty_string = Get_No_empty_String(category);
				String Filter = category_no_empty_string+"Filter";
				FilterList.add(Filter);
			}
			String Filters = FilterList.toString();
			Filters= Filters.substring(1,Filters.length()-1);

			int eof_first_column= Common_Utils.getEOF_Row_For_Each_Column(xssfSheet,0);
			for(int i=0;i<eof_first_column;i++){
				XSSFRow row =xssfSheet.getRow(i);
				if(row==null) break;
				XSSFCell category_cell = row.getCell(0);
				if(category_cell==null) break;
				String category=category_cell.toString().trim();
				if(category.length()<2) break;
				if(category.substring(0,1).equals("O")){
					category= category.substring(1);
				}
				category= Get_String(category);

				//category VMwareEmbeddedVirtualSANCategory extends Category {
				//	description		    =	"VMware Virtual SAN Embedded(Add-on)";
				//	softwareType		=	perNode;
				//	orderPriority		=   328;
				//	selectionsFilters	=	VMwareEmbeddedVirtualSANFilter,EntitlementTypeAddonFilter,EntitlementDurationFilter;
				//	minMaxDefaultQty    =   "1:10:1";  
				//}
				String category_no_empty_string = Get_No_empty_String(category);
				stringBuffer.append("//"+category+"\n");
				stringBuffer.append("category "+ Get_No_empty_String(category)+"Category extends Category{\n");
				stringBuffer.append("     description    	   =   \""+category+"\";\n");
				stringBuffer.append("     softwareType         =   perNode;\n");
				stringBuffer.append("     orderPriority        =   328;\n");
				stringBuffer.append("     selectionsFilters    =   "+Filters+";\n");
				stringBuffer.append("}\n");

				stringBuffer.append("\n");
				//class VMwareEmbeddedVirtualSAN extends Software{  	
				//    category	 = VMwareEmbeddedVirtualSANCategory;
				//}
				stringBuffer.append("class "+ Get_No_empty_String(category)+" extends Software{\n");
				stringBuffer.append("     category    =   "+Get_No_empty_String(category)+"Category"+";\n");
				stringBuffer.append("}\n");

				stringBuffer.append("\n");
				stringBuffer.append("\n");
			}


			//     //*****************定义Filter***************//
			//	   //*****************定义Filter***************//
			//int eof_column= Common_Utils.getEOF_Column(xssfSheet,0);
			//System.out.println("eof_column: "+eof_column);

			for(int i=0;i<eof_column;i++){

				int eof_row= Common_Utils.getEOF_Row_For_Each_Column(xssfSheet,i);
				System.out.println("eof_row: "+eof_row);
				ArrayList<String>  ss = new ArrayList<>();
				for(int j=1;j<eof_row;j++){
					XSSFRow row =xssfSheet.getRow(j);
					if(row==null) break;
					XSSFCell category_cell = row.getCell(i);
					if(category_cell==null) break;
					String category=category_cell.toString().trim();
					if(category.length()<2) break;

					category= Get_String(category);
					String category_no_empty_string = Get_No_empty_String(category);
					String s = category_no_empty_string+"FilterItem";
					ss.add(s);
					//categoryIBMCognosAnalyticsAdministratorFilterItem extends FilterItem{
					//   	description		    =	"IBM Cognos Analytics Administrator";
					//}
				}

				String FilterItems = ss.toString();
				FilterItems= FilterItems.substring(1,FilterItems.length()-1);
				XSSFRow  Filter_row =xssfSheet.getRow(0); 
				if(Filter_row==null) break;
				XSSFCell Filter_cell = Filter_row.getCell(i);//找到第一行某一列作为Filter
				if(Filter_cell==null) break;
				String FilterString=Filter_cell.toString().trim();
				FilterString= Get_String(FilterString);
				if(FilterString.length()<2) break;
				//String FilterString =xssfSheet.getRow(i).getCell(0).toString().trim();
				stringBuffer.append("//"+Get_String(FilterString)+"\n");
				stringBuffer.append("category "+ Get_No_empty_String(FilterString)+"Filter extends Filter{\n");
				stringBuffer.append("     description    =   \""+FilterString+"\";\n");
				stringBuffer.append("     filterItems    =   "+FilterItems+";\n");
				stringBuffer.append("}\n");



				//     //*****************定义FilterItem***************//
				//	   //*****************定义FilterItem***************//
				for(int j=1;j<eof_row;j++){
					XSSFRow row =xssfSheet.getRow(j);
					if(row==null) break;
					XSSFCell category_cell = row.getCell(i);
					if(category_cell==null) break;
					String category=category_cell.toString().trim();
					if(category.length()<2) break;
					if(category.substring(0,1).equals("O")){
						category= category.substring(1);
					}
					category= Get_String(category);
					String category_no_empty_string = Get_No_empty_String(category);

					stringBuffer.append("//"+category+"\n");
					stringBuffer.append("category "+ category_no_empty_string+"FilterItem extends FilterItem{\n");
					stringBuffer.append("     description    =   \""+category+"\";\n");
					stringBuffer.append("}\n");
					//categoryIBMCognosAnalyticsAdministratorFilterItem extends FilterItem{
					//   	description		    =	"IBM Cognos Analytics Administrator";
					//}
				}
				//stringBuffer.append("\n"+ss.toString()+"\n\n");
				for(int k=0;k<ss.size();k++){
					System.out.println(ss.get(k));
					stringBuffer.append(ss.get(k)+"\n");
				}

				stringBuffer.append("\n\n");
			}
			//System.out.println(ss.toString());


			saveDataToFile3(stringBuffer.toString(),"D:/1_SW/Generate_FilterItem.txt");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void generate_SQL_create_PUID(String path){
		String end_file =path.substring(path.length()-4,path.length());
		if(end_file.equals("xlsx")){
			//get_FCs_from_Source_File(path);
			get_FCs_from_Source_File_Public(path);
		}else if(end_file.equals(".xls")){
			get_FCs_from_Source_File_2003(path);
		}
	}
	/*
	public void set_map_for_Service(String path){
		XSSFWorkbook xssfWorkbook;
		try {
			xssfWorkbook = new XSSFWorkbook(path);

			XSSFSheet xssfSheet = xssfWorkbook.getSheet("FC_PUID");
			if(xssfSheet==null){
				xssfSheet = xssfWorkbook.getSheetAt(0);
			}
			if(xssfSheet==null){
				System.out.println("The Sheet is not Exist");
			}
			StringBuffer stringBuffer = new StringBuffer();
			int eof_row= Common_Utils.getEOF_Row(xssfSheet, xssfSheet.getLastRowNum()+1);
			for(int i=1;i<eof_row;i++){
				XSSFRow row =xssfSheet.getRow(i);
				if(row==null) continue;
				XSSFCell FC_cell = row.getCell(0);
				String FC=FC_cell.toString();
				XSSFCell PUID_cell = row.getCell(1);
				String PUID=PUID_cell.toString();
				if(PUID.contains(".")){
					System.out.println("kkkkkkkkk");
					PUID=PUID.substring(0,PUID.lastIndexOf("."));
				}
				//CTO_FC_Map.put("7Y42CTO1WW","148802");
				System.out.println("CTO_FC_Map.put(\""+FC+"\",\""+PUID+"\");\n");
				stringBuffer.append("CTO_FC_Map.put(\""+FC+"\",\""+PUID+"\");\n");
			}
			stringBuffer.append("\n");
			//"C:/Users/xiexg1/Desktop/Service/Service_Search_PUID_2.9.xlsx"
			saveDataToFile3(stringBuffer.toString(),"C:/Users/xiexg1/Desktop/Service/Service_Source_FC_PUID.txt");
			//System.out.println("Software_FCs List Size: "+Software_FCs.size());

		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	 */

	/*
	@SuppressWarnings("deprecation")
	public void generate_CTO_FC_Item(String path){
		XSSFWorkbook xssfWorkbook;
		try {
			xssfWorkbook = new XSSFWorkbook(path);

			XSSFSheet xssfSheet = xssfWorkbook.getSheet("FC");
			if(xssfSheet==null){
				xssfSheet = xssfWorkbook.getSheetAt(0);
			}
			if(xssfSheet==null){
				System.out.println("The Sheet is not Exist");
			}

			ArrayList<String>  ss = new ArrayList<>();
			int eof_row= Common_Utils.getEOF_Row(xssfSheet, xssfSheet.getLastRowNum()+1);


			for(int i=1;i<eof_row;i++){
				XSSFRow row =xssfSheet.getRow(i);
				if(row==null) continue;
				XSSFCell Content_cell = row.getCell(0);
				String Content=Content_cell.toString();
				//System.out.println(Content);
				Content = Content.replaceAll(" " , "");
				List<String> list = extractMessage(Content);
				for (int j = 0; j < list.size(); j++) {
					//System.out.println(i+"-->"+list.get(j));
					if(!ss.contains(list.get(j))){
						ss.add(list.get(j));
					}
				}
				//categoryIBMCognosAnalyticsAdministratorFilterItem extends FilterItem{
				//   	description		    =	"IBM Cognos Analytics Administrator";
				//}
			}
			//System.out.println("Software_FCs List Size: "+Software_FCs.size());

			System.out.println(ss.toString());
			for (int j = 0; j < ss.size(); j++) {
				System.out.println(ss.get(j));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	 */
	/*
	@SuppressWarnings("deprecation")
	public void get_PUID_DB_for_Source_File(String path){
		XSSFWorkbook xssfWorkbook;

		try {
			FileOutputStream fout = new FileOutputStream("C:/Users/xiexg1/Desktop/Service/Service_Search_PUID_3.0.xlsx");
			xssfWorkbook = new XSSFWorkbook(path);

			XSSFSheet xssfSheet = xssfWorkbook.getSheet("Services");
			if(xssfSheet==null){
				xssfSheet = xssfWorkbook.getSheetAt(0);
			}
			if(xssfSheet==null){
				System.out.println("The Sheet is not Exist");
			}

			String content="";
			int eof_row= Common_Utils.getEOF_Row(xssfSheet, xssfSheet.getLastRowNum()+1);
			for(int i=1;i<eof_row;i++){
				XSSFRow row =xssfSheet.getRow(i);
				if(row==null) continue;
				XSSFCell FC_cell = row.getCell(1);
				String FC=FC_cell.toString();
				content += "'"+FC + "',";
			}
			content=content.substring(0,content.length()-1);
			String sql="select PUID,OPT_PN from DB2INST1.PRODUCT  where OPT_PN in("+content.trim()+")";
			System.out.println("select PUID,OPT_PN from DB2INST1.PRODUCT  where OPT_PN in("+content.trim()+")");

			Map<String,String> maps = query_FC_Under_FeMale(sql);
			System.out.println("maps.size: "+maps.size());
			Thread.sleep(1000);
			for(int i=1;i<eof_row;i++){
				XSSFRow row =xssfSheet.getRow(i);
				if(row==null) continue;
				XSSFCell FC_cell = row.getCell(1);
				String FC=FC_cell.toString();
				XSSFCell cell=row.createCell(0);
				System.out.println(maps.get(FC));
				cell.setCellValue(maps.get(FC));
			}
			fout.flush();
			xssfWorkbook.write(fout);
			fout.close();

		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
	 */
	/*
	@SuppressWarnings("deprecation")
	public void generate_AML_Code_for_Service(String path){
		XSSFWorkbook xssfWorkbook;
		try {
			xssfWorkbook = new XSSFWorkbook(path);

			XSSFSheet xssfSheet = xssfWorkbook.getSheet("Services");
			if(xssfSheet==null){
				xssfSheet = xssfWorkbook.getSheetAt(0);
			}
			if(xssfSheet==null){
				System.out.println("The Sheet is not Exist");
			}

			//ArrayList<String>  ss = new ArrayList<>();
			int eof_row= Common_Utils.getEOF_Row(xssfSheet, xssfSheet.getLastRowNum()+1);
			Map<String, String>  CTO_FC_Map = get_map();
			int count=0;
			int rule_count=0;
			StringBuffer stringBuffer = new StringBuffer();
			String except_FCs="B470,B471,B472,B473,B474";
			for(int i=1;i<eof_row;i++){
				XSSFRow row =xssfSheet.getRow(i);
				if(row==null) continue;
				XSSFCell PUID_cell = row.getCell(0);
				String PUID_String=PUID_cell.toString();
				XSSFCell PN_cell = row.getCell(1);
				String PN_String=PN_cell.toString();
				XSSFCell DESC_cell = row.getCell(2);
				String DESC_String=DESC_cell.toString();
				XSSFCell Rule_cell = row.getCell(3);
				String Rule_String_raw=Rule_cell.toString();
				String Rule_String = Rule_String_raw.replaceAll(" " , "");

				//System.out.println(Rule_String);

				//int Deriveon_start_index_raw=Rule_String_raw.indexOf("Derive");
				int Deriveon_end_index_raw=Rule_String_raw.lastIndexOf("Derive");

				int Deriveon_start_index=Rule_String.indexOf("Deriveon");
				int Deriveon_end_index=Rule_String.lastIndexOf("Deriveon");

				boolean two_conditions= false;
				if(Deriveon_start_index!=Deriveon_end_index){
					//System.out.println(PN_String+":  "+Rule_String);
					//					String rule1_string= Rule_String.substring(0, Deriveon_end_index);
					//					String rule2_string= Rule_String.substring(Deriveon_end_index);
					//					System.out.println("rule1_string: "+rule1_string);
					//					System.out.println("rule2_string: "+rule2_string);
					two_conditions= true;
					count++;
				}

				if(!two_conditions){
					rule_count++;
					List<String> list = extractMessage(Rule_String);
					//System.out.println("//"+PUID_String+"\t"+PN_String+"\t"+DESC_String);
					//System.out.println("//"+Rule_String_raw);
					stringBuffer.append("//"+PUID_String+"\t"+PN_String+"\t"+DESC_String+"\n");
					stringBuffer.append("//"+Rule_String_raw+"\n");


					DESC_String = DESC_String.replaceAll(" " , "");
					DESC_String = DESC_String.replaceAll("-" , "");
					String CTO_PN_String="";
					String CTO_Puid="";
					String sourcePuids="";
					for (int j = 0; j < list.size(); j++) {
						String value= list.get(j);
						if(value.contains("7Y")&&value.length()>5){
							CTO_Puid=CTO_FC_Map.get(value);
							CTO_PN_String=PN_String+"_"+value;
						}
						if(except_FCs.contains(value)) continue;
						if((!value.contains("Japan")&&(!value.contains("China")))){
							sourcePuids+="1x"+CTO_FC_Map.get(value)+",";
						}
					}
					String rule_name ="autoAdd "+CTO_PN_String+"_SVC_WW extends ConditionalAdd {";
					sourcePuids=sourcePuids.substring(0,sourcePuids.length()-1);
					System.out.println(rule_name);
					System.out.println("    sourcePuids         = "+sourcePuids+";");
					System.out.println("    destinationPuid     = "+CTO_Puid+";");
					System.out.println("    resultPuid          = "+PUID_String+";");
					System.out.println("    resultQty           = 1;");
					System.out.println("}");

					stringBuffer.append(rule_name+"\n");
					stringBuffer.append("    sourcePuids         = "+sourcePuids+";"+"\n");
					stringBuffer.append("    destinationPuid     = "+CTO_Puid+";"+"\n");
					stringBuffer.append("    resultPuid          = "+PUID_String+";"+"\n");
					stringBuffer.append("    resultQty           = 1;"+"\n");
					stringBuffer.append("}"+"\n");

				}

				if(two_conditions){
					rule_count+=2;
					String rule1_string= Rule_String.substring(0, Deriveon_end_index);
					String rule2_string= Rule_String.substring(Deriveon_end_index);
					String rule1_string_raw= Rule_String_raw.substring(0, Deriveon_end_index_raw);
					String rule2_string_raw= Rule_String_raw.substring(Deriveon_end_index_raw);					

					System.out.println("//"+PUID_String+"\t"+PN_String+"\t"+DESC_String);
					System.out.println("//"+Rule_String_raw);
					stringBuffer.append("//"+PUID_String+"\t"+PN_String+"\t"+DESC_String+"\n");
					stringBuffer.append("//"+rule1_string_raw);
					stringBuffer.append("//"+rule2_string_raw+"\n");
					List<String> list1 = extractMessage(rule1_string);
					DESC_String = DESC_String.replaceAll(" " , "");
					DESC_String = DESC_String.replaceAll("-" , "");

					String CTO_Puid="";
					String sourcePuids="";
					String CTO_PN_String_1="";
					for (int j = 0; j < list1.size(); j++) {
						String value= list1.get(j);
						if(value.contains("7Y")&&value.length()>5){
							CTO_Puid=CTO_FC_Map.get(value);
							CTO_PN_String_1=PN_String+"_"+value;
						}
						if(except_FCs.contains(value)) continue;
						if((!value.contains("Japan")&&(!value.contains("China")))){
							sourcePuids+="1x"+CTO_FC_Map.get(value)+",";
						}
					}
					String rule_name ="autoAdd "+CTO_PN_String_1+"_SVC_WW_1 extends ConditionalAdd {";
					sourcePuids=sourcePuids.substring(0,sourcePuids.length()-1);
					System.out.println(rule_name);
					System.out.println("    sourcePuids         = "+sourcePuids+";");
					System.out.println("    destinationPuid     = "+CTO_Puid+";");
					System.out.println("    resultPuid          = "+PUID_String+";");
					System.out.println("    resultQty           = 1;");
					System.out.println("}");
					stringBuffer.append(rule_name+"\n");
					stringBuffer.append("    sourcePuids         = "+sourcePuids+";"+"\n");
					stringBuffer.append("    destinationPuid     = "+CTO_Puid+";"+"\n");
					stringBuffer.append("    resultPuid          = "+PUID_String+";"+"\n");
					stringBuffer.append("    resultQty           = 1;"+"\n");
					stringBuffer.append("}"+"\n");

					List<String> list2 = extractMessage(rule2_string);
					DESC_String = DESC_String.replaceAll(" " , "");
					DESC_String = DESC_String.replaceAll("-" , "");

					String CTO_Puid2="";
					String sourcePuids2="";
					String CTO_PN_String_2="";
					for (int j = 0; j < list2.size(); j++) {
						String value= list2.get(j);
						if(value.contains("7Y")&&value.length()>5){
							CTO_Puid2=CTO_FC_Map.get(value);
							CTO_PN_String_2=PN_String+"_"+value;
						}
						if(except_FCs.contains(value)) continue;
						if((!value.contains("Japan")&&(!value.contains("China")))){
							sourcePuids2+="1x"+CTO_FC_Map.get(value)+",";
						}
					}
					String rule_name2 ="autoAdd "+CTO_PN_String_2+"_SVC_WW_2 extends ConditionalAdd {";
					sourcePuids2=sourcePuids2.substring(0,sourcePuids2.length()-1);
					System.out.println(rule_name2);
					System.out.println("    sourcePuids         = "+sourcePuids2+";");
					System.out.println("    destinationPuid     = "+CTO_Puid2+";");
					System.out.println("    resultPuid          = "+PUID_String+";");
					System.out.println("    resultQty           = 1;");
					System.out.println("}");
					stringBuffer.append(rule_name2+"\n");
					stringBuffer.append("    sourcePuids         = "+sourcePuids2+";"+"\n");
					stringBuffer.append("    destinationPuid     = "+CTO_Puid2+";"+"\n");
					stringBuffer.append("    resultPuid          = "+PUID_String+";"+"\n");
					stringBuffer.append("    resultQty           = 1;"+"\n");
					stringBuffer.append("}"+"\n");
				}


				//autoAdd DM3000H_Fondation_3Year_B3X4_SVC_WW extends ConditionalAdd { 
				//       sourcePuids         = 1x148802, 1x149881, 1x149871, 1x158853; 
				//       destinationPuid     = 148802; 
				//       resultPuid          = <PUID of 5WS7A18141>; 
				//       resultQty           = 1; 
				//}

			}
			//System.out.println("Software_FCs List Size: "+Software_FCs.size());
			System.out.println("count: " +count);
			System.out.println("rule_count: " +rule_count);
			saveDataToFile2(stringBuffer.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	 */

	/*
	public  List<String> extractMessage(String msg) {
		List<String> list = new ArrayList<String>();
		int start = 0;
		int startFlag = 0;
		int endFlag = 0;
		for (int i = 0; i < msg.length(); i++) {
			if (msg.charAt(i) == '(') {
				startFlag++;
				if (startFlag == endFlag + 1) {
					start = i;
				}
			} else if (msg.charAt(i) == ')') {
				endFlag++;
				if (endFlag == startFlag) {
					list.add(msg.substring(start + 1, i));
				}
			}
		}
		return list;
	}
	 */
	/*
	public Map<String,String> get_map(){
		Map<String, String>  CTO_FC_Map = new HashMap<String,String>();
		CTO_FC_Map.put("7Y40CTO1WW","148800");
		CTO_FC_Map.put("7Y41CTO1WW","148801");
		CTO_FC_Map.put("7Y42CTO1WW","148802");
		CTO_FC_Map.put("7Y43CTO1WW","148803");
		CTO_FC_Map.put("7Y56CTO1WW","148865");
		CTO_FC_Map.put("7Y57CTO1WW","148828");
		CTO_FC_Map.put("7Y58CTO1WW","148862");
		CTO_FC_Map.put("7Y59CTO1WW","148866");
		CTO_FC_Map.put("B0W1","149871");
		CTO_FC_Map.put("B0W2","149872");
		CTO_FC_Map.put("B3W2","148847");
		CTO_FC_Map.put("B3W3","148838");
		CTO_FC_Map.put("B3W5","148870");
		CTO_FC_Map.put("B3W6","148871");
		CTO_FC_Map.put("B3W7","148848");
		CTO_FC_Map.put("B3W8","148839");
		CTO_FC_Map.put("B3WC","148875");
		CTO_FC_Map.put("B3WD","148876");
		CTO_FC_Map.put("B3WE","148849");
		CTO_FC_Map.put("B3WF","148840");
		CTO_FC_Map.put("B3WJ","148879");
		CTO_FC_Map.put("B3WK","148850");
		CTO_FC_Map.put("B3WL","148841");
		CTO_FC_Map.put("B3WM","148880");
		CTO_FC_Map.put("B3WN","148881");
		CTO_FC_Map.put("B3WP","148882");
		CTO_FC_Map.put("B3WQ","148851");
		CTO_FC_Map.put("B3WR","148842");
		CTO_FC_Map.put("B3WS","148883");
		CTO_FC_Map.put("B3WT","148884");
		CTO_FC_Map.put("B3WU","148885");
		CTO_FC_Map.put("B3WV","148852");
		CTO_FC_Map.put("B3WW","148843");
		CTO_FC_Map.put("B3WX","148886");
		CTO_FC_Map.put("B3WY","148844");
		CTO_FC_Map.put("B3WZ","148887");
		CTO_FC_Map.put("B3X0","148845");
		CTO_FC_Map.put("B3X1","148888");
		CTO_FC_Map.put("B3X2","148846");
		CTO_FC_Map.put("B3X3","148889");
		CTO_FC_Map.put("B3X4","148853");
		CTO_FC_Map.put("B3X5","148897");
		CTO_FC_Map.put("B3X6","148854");
		CTO_FC_Map.put("B3X7","148898");
		CTO_FC_Map.put("B3X8","148855");
		CTO_FC_Map.put("B3X9","148899");
		CTO_FC_Map.put("B3XA","148856");
		CTO_FC_Map.put("B3XB","148857");
		CTO_FC_Map.put("B3XC","148858");
		CTO_FC_Map.put("B3XD","148859");
		CTO_FC_Map.put("B3XE","148860");
		CTO_FC_Map.put("B3XF","148861");
		CTO_FC_Map.put("B46X","149880");
		CTO_FC_Map.put("B46Y","149881");
		CTO_FC_Map.put("B470","149866");
		CTO_FC_Map.put("B471","149867");
		CTO_FC_Map.put("B472","149868");
		CTO_FC_Map.put("B473","149869");
		CTO_FC_Map.put("B474","149889");
		CTO_FC_Map.put("B48J","149882");
		CTO_FC_Map.put("B48K","149884");	
		CTO_FC_Map.put("B2UW","150972");

		CTO_FC_Map.put("B5QE","158686");
		CTO_FC_Map.put("B5QF","158687");
		CTO_FC_Map.put("B5QG","158688");
		CTO_FC_Map.put("B5QH","158689");
		CTO_FC_Map.put("B5QJ","158690");
		CTO_FC_Map.put("B5QK","158691");
		CTO_FC_Map.put("B5QL","158692");
		CTO_FC_Map.put("B5QM","158693");
		CTO_FC_Map.put("B5QN","158694");
		CTO_FC_Map.put("B5RH","159839");
		CTO_FC_Map.put("B5Q8","159841");
		CTO_FC_Map.put("B5Q9","159842");
		CTO_FC_Map.put("B5QA","159843");
		CTO_FC_Map.put("B5QB","159844");
		CTO_FC_Map.put("B5QC","159845");
		CTO_FC_Map.put("B5QD","159846");
		CTO_FC_Map.put("B5QP","159847");
		CTO_FC_Map.put("B5QQ","159848");
		CTO_FC_Map.put("B5QR","159849");
		CTO_FC_Map.put("B5R8","159850");
		CTO_FC_Map.put("B5R9","159851");
		CTO_FC_Map.put("B5RA","159852");
		CTO_FC_Map.put("B5RB","159853");
		CTO_FC_Map.put("B5RC","159854");
		CTO_FC_Map.put("B5RD","159855");
		CTO_FC_Map.put("B5RE","159857");
		CTO_FC_Map.put("B5RF","159858");
		CTO_FC_Map.put("B5RG","159859");
		CTO_FC_Map.put("B5QS","159861");
		CTO_FC_Map.put("B5QT","159862");
		CTO_FC_Map.put("B5QU","159863");
		CTO_FC_Map.put("B5QV","159864");
		CTO_FC_Map.put("B5QW","159865");
		CTO_FC_Map.put("B5QX","159866");
		CTO_FC_Map.put("B5QY","159867");
		CTO_FC_Map.put("B5QZ","159868");
		CTO_FC_Map.put("B5R0","159869");
		CTO_FC_Map.put("B5R1","159870");
		CTO_FC_Map.put("B5R2","160214");
		CTO_FC_Map.put("B5R4","160216");
		CTO_FC_Map.put("B5R5","160217");
		CTO_FC_Map.put("B5R6","160219");
		CTO_FC_Map.put("B5R7","160221");
		return CTO_FC_Map;
	}

	 */


	/*
	private Map<String, String> query_FC_Under_FeMale(String sql) throws SQLException {

		Map<String,String> maps = new HashMap<>();
		Network.connectDatabase();
		ResultSet FC_rs_Under_FeMale=Network.stmt.executeQuery(sql);

		while(FC_rs_Under_FeMale.next())
		{
			//System.out.println(FC_rs_Under_FeMale.getString(2).toString());
			maps.put(FC_rs_Under_FeMale.getString(2).toString(), FC_rs_Under_FeMale.getString(1).toString());
		}
		FC_rs_Under_FeMale.close();

		return maps;	
	}
	 */

	@SuppressWarnings("deprecation")
	public void get_FCs_from_Source_File(String path){
		XSSFWorkbook xssfWorkbook;
		try {
			xssfWorkbook = new XSSFWorkbook(path);

			XSSFSheet xssfSheet = xssfWorkbook.getSheet("Pivot3 Mapping");
			if(xssfSheet==null){
				xssfSheet = xssfWorkbook.getSheetAt(0);
			}
			if(xssfSheet==null){
				System.out.println("The Sheet is not Exist");
			}
			//Loadsheet_Title_Row = get_Title_Row(xssfSheet);
			Loadsheet_FC_Column=get_FC_Column_from_Loadsheet(xssfSheet,"FC");
			//Loadsheet_PN_Column=get_FC_Column_from_Loadsheet(xssfSheet,"LFO");
			Loadsheet_PN_Column=get_FC_Column_from_Loadsheet(xssfSheet,"PN");
			Loadsheet_DESC_Column=get_FC_Column_from_Loadsheet(xssfSheet,"Description");

			System.out.println("Loadsheet_FC_Column: "+Loadsheet_FC_Column);

			int eof_row= Common_Utils.getEOF_Row(xssfSheet, xssfSheet.getLastRowNum()+1);
			for(int i=1;i<eof_row;i++){
				XSSFRow row =xssfSheet.getRow(i);
				if(row==null) continue;
				XSSFCell FC_cell = row.getCell(Loadsheet_FC_Column);
				String FC=FC_cell.toString();
				XSSFCell PN_cell = row.getCell(Loadsheet_PN_Column);
				String PN=PN_cell.toString();
				XSSFCell DESC_cell = row.getCell(Loadsheet_DESC_Column);
				String DESC="";
				if(DESC_cell==null){
					DESC="";
				}else{
					DESC=DESC_cell.toString();
				}

				Software_FC Software_FC=	new Software_FC();
				Software_FC.setDescription(DESC);
				if(PN.contains("AAAAA")||PN.contains("N/A")){
					Software_FC.setOPT_PN("");
				}else{
					Software_FC.setOPT_PN(PN);
				}
				if(FC.contains("AAAAA")||FC.contains("N/A")){
					Software_FC.setFC("");
				}else{
					Software_FC.setFC(FC);
				}
				Software_FCs.add(Software_FC);
			}
			System.out.println("Software_FCs List Size: "+Software_FCs.size());

			Save_SQL_to_TXT();


		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@SuppressWarnings("deprecation")
	public void get_FCs_from_Source_File_Public(String path){
		Workbook xssfWorkbook;
		try {
			//xssfWorkbook = new XSSFWorkbook(path);

			InputStream inputStream=new FileInputStream(new File(path));
			xssfWorkbook = WorkbookFactory.create(inputStream);

			Sheet xssfSheet = xssfWorkbook.getSheet("FC");
			if(xssfSheet==null){
				xssfSheet = xssfWorkbook.getSheetAt(0);
			}
			if(xssfSheet==null){
				System.out.println("The Sheet is not Exist");
			}
			//Loadsheet_Title_Row = get_Title_Row(xssfSheet);
			Loadsheet_FC_Column=get_FC_Column_from_Loadsheet_Public(xssfSheet,"FC");
			//Loadsheet_PN_Column=get_FC_Column_from_Loadsheet(xssfSheet,"LFO");
			Loadsheet_PN_Column=get_FC_Column_from_Loadsheet_Public(xssfSheet,"PN");
			Loadsheet_DESC_Column=get_FC_Column_from_Loadsheet_Public(xssfSheet,"Description");

			System.out.println("Loadsheet_FC_Column: "+Loadsheet_FC_Column);

			int eof_row= Common_Utils.getEOF_Row_public(xssfSheet, xssfSheet.getLastRowNum()+1);
			for(int i=1;i<eof_row;i++){
				Row row =xssfSheet.getRow(i);
				if(row==null) continue;
				Cell FC_cell = row.getCell(Loadsheet_FC_Column);
				String FC=FC_cell.toString();
				Cell PN_cell = row.getCell(Loadsheet_PN_Column);
				String PN=PN_cell.toString();
				Cell DESC_cell = row.getCell(Loadsheet_DESC_Column);
				String DESC="";
				if(DESC_cell==null){
					DESC="";
				}else{
					DESC=DESC_cell.toString();
				}

				Software_FC Software_FC=	new Software_FC();
				Software_FC.setDescription(DESC);
				if(PN.contains("AAAAA")||PN.contains("N/A")){
					Software_FC.setOPT_PN("");
				}else{
					Software_FC.setOPT_PN(PN);
				}
				if(FC.contains("AAAAA")||FC.contains("N/A")){
					Software_FC.setFC("");
				}else{
					Software_FC.setFC(FC);
				}
				Software_FCs.add(Software_FC);
			}
			System.out.println("Software_FCs List Size: "+Software_FCs.size());

			Save_SQL_to_TXT();
			inputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void get_FCs_from_Source_File_2003(String path){
		HSSFWorkbook hssfWorkbook;
		try {

			hssfWorkbook = new HSSFWorkbook(new FileInputStream(new File(path)));

			HSSFSheet hssfSheet = hssfWorkbook.getSheet("Pivot3 Mapping");
			if(hssfSheet==null){
				System.out.println("The Sheet is not Exist");
			}
			//Loadsheet_Title_Row = get_Title_Row(xssfSheet);
			Loadsheet_FC_Column=get_FC_Column_from_Loadsheet_2003(hssfSheet,"FC");
			Loadsheet_PN_Column=get_FC_Column_from_Loadsheet_2003(hssfSheet,"LFO");
			Loadsheet_DESC_Column=get_FC_Column_from_Loadsheet_2003(hssfSheet,"Description");

			System.out.println("Loadsheet_FC_Column: "+Loadsheet_FC_Column);
			for(int i=1;i<hssfSheet.getLastRowNum()+1;i++){
				HSSFRow row =hssfSheet.getRow(i);
				if(row==null) continue;
				HSSFCell FC_cell = row.getCell(Loadsheet_FC_Column);
				String FC=FC_cell.toString();
				HSSFCell PN_cell = row.getCell(Loadsheet_PN_Column);
				String PN=PN_cell.toString();
				HSSFCell DESC_cell = row.getCell(Loadsheet_DESC_Column);
				String DESC="";
				if(DESC_cell==null){
					DESC="";
				}else{
					DESC=DESC_cell.toString();
				}
				Software_FC Software_FC=	new Software_FC();
				Software_FC.setDescription(DESC);
				if(PN.contains("AAAAA")||PN.contains("N/A")){
					Software_FC.setOPT_PN("");
				}else{
					Software_FC.setOPT_PN(PN);
				}
				if(FC.contains("AAAAA")||FC.contains("N/A")){
					Software_FC.setFC("");
				}else{
					Software_FC.setFC(FC);
				}
				Software_FCs.add(Software_FC);
			}
			System.out.println("Software_FCs List Size: "+Software_FCs.size());

			Save_SQL_to_TXT();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}


	//INSERT INTO DB2INST1.PRODUCT(DESCRIPTION,FT_UID,CAT_ID,MFG_ID,BID,FEAT_CODE,OPT_PN,VISIBLE,PASSTHRU,PRICED) values (	'	Pivot3 Acuity Hybrid Standard 8TB Lic w/3Yr Premium Support	',	16	,	361	,	1	,	1	,	'	B2HK	',	'	7S000025WW	',	1	,	0	,	'	Y	'	);
	public void Save_SQL_to_TXT(){
		StringBuilder Sumary = new StringBuilder();
		for(int i=0;i<Software_FCs.size();i++){
			Software_FC FC=Software_FCs.get(i);

			// String SQL_Start="INSERT INTO DB2INST1.PRODUCT(DESCRIPTION,FT_UID,CAT_ID,MFG_ID,BID,FEAT_CODE,OPT_PN,VISIBLE,PASSTHRU,PRICED) values ('";
			//',	16	,	361	,	1	,	1	,	'	B2HK	',	'	7S000025WW	',	1	,	0	,	'	Y	'	);
			// String SQL_Parameter_1="\',16,361,1,1,'";
			// String SQL_Parameter_2="\',1,0,\'Y\')";
			//String s = SQL_Start+DESC+SQL_Parameter_1+FC_name+"\',\'"+PN+SQL_Parameter_2;
			String s = SQL_Start+FC.getDescription()+SQL_Parameter_1+FC.getFC()+"\',\'"+FC.getOPT_PN()+SQL_Parameter_2+"\n";

			System.out.println(s);
			Sumary.append(s);
		}

		String Select_SQL="";
		for (Software_FC Software_FC:Software_FCs) {
			Select_SQL += "'"+Software_FC.getFC().toString() + "',";
		}
		Select_SQL=Select_SQL.substring(0,Select_SQL.length()-1);
		String Select_SQL_2="select PUID,FEAT_CODE,DESCRIPTION,OPT_PN from DB2INST1.PRODUCT  where CAT_ID = '361' and FEAT_CODE in("+Select_SQL.trim()+")";
		Select_SQL="select * from DB2INST1.PRODUCT  where CAT_ID = '361' and FEAT_CODE in("+Select_SQL.trim()+")";
		Sumary.append("\n\n");
		Sumary.append(Select_SQL+"\n");
		Sumary.append(Select_SQL_2);
		saveDataToFile(Sumary.toString());
	}


	public void saveDataToFile (String string) {

		try {
			//			String Folder_path=path.substring(0, path.lastIndexOf("\\"));
			//			String out_put_path_folder = Folder_path+"\\"+Update_String;
			//			File out_folder = new File(out_put_path_folder);
			//			if(!out_folder.exists()){
			//				out_folder.mkdir();
			//			}
			File file = new File("D:/1_SW/Software_SQL_Create_PUID.txt");
			if (!file.exists()) {
				file.createNewFile();
			}
			FileOutputStream outStream = new FileOutputStream(file);
			outStream.write(string.trim().getBytes());
			outStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	/*
	public static void saveDataToFile2 (String string) {
		try {
			File file = new File("D:/Service.txt");

			if (!file.exists()) {
				file.createNewFile();
			}
			FileOutputStream outStream = new FileOutputStream(file);
			outStream.write(string.trim().getBytes());
			outStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	 */
	public static void saveDataToFile3 (String string,String Path) {
		try {
			//File file = new File("D:/CombineFC2.txt");
			File file = new File(Path);
			if (!file.exists()) {
				file.createNewFile();
			}
			FileOutputStream outStream = new FileOutputStream(file);
			outStream.write(string.trim().getBytes());
			outStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
