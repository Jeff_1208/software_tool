package entity;

public class Software_FC {

	private String Description;
	private String FC;
	private String OPT_PN;
	private String CTO;
	private String Subcat;
	private String Model;
	private String Type;
	private String Duration;
	private boolean is_8TB=false;
	private boolean is_12TB=false;
	private boolean is_16TB=false;
	private boolean is_24TB=false;
	private boolean is_48TB=false;
	private boolean is_72TB=false;
	private boolean is_96TB=false;
	private boolean is_120TB=false;
	private boolean is_144TB=false;
	private boolean is_3200TB=false;
	private boolean is_6400TB=false;
	private boolean is_7600TB=false;
	private boolean is_12800TB=false;
	private boolean is_15300TB=false;
	private boolean is_30700TB=false;
	private boolean is_61400TB=false;
	
	public String getCTO() {
		return CTO;
	}
	public void setCTO(String cTO) {
		CTO = cTO;
	}
	public String getSubcat() {
		return Subcat;
	}
	public void setSubcat(String subcat) {
		Subcat = subcat;
	}
	public String getModel() {
		return Model;
	}
	public void setModel(String model) {
		Model = model;
	}
	public String getType() {
		return Type;
	}
	public void setType(String type) {
		Type = type;
	}
	public String getDuration() {
		return Duration;
	}
	public void setDuration(String duration) {
		Duration = duration;
	}
	public boolean isIs_8TB() {
		return is_8TB;
	}
	public void setIs_8TB(boolean is_8tb) {
		is_8TB = is_8tb;
	}
	public boolean isIs_12TB() {
		return is_12TB;
	}
	public void setIs_12TB(boolean is_12tb) {
		is_12TB = is_12tb;
	}
	public boolean isIs_16TB() {
		return is_16TB;
	}
	public void setIs_16TB(boolean is_16tb) {
		is_16TB = is_16tb;
	}
	public boolean isIs_24TB() {
		return is_24TB;
	}
	public void setIs_24TB(boolean is_24tb) {
		is_24TB = is_24tb;
	}
	public boolean isIs_48TB() {
		return is_48TB;
	}
	public void setIs_48TB(boolean is_48tb) {
		is_48TB = is_48tb;
	}
	public boolean isIs_72TB() {
		return is_72TB;
	}
	public void setIs_72TB(boolean is_72tb) {
		is_72TB = is_72tb;
	}
	public boolean isIs_96TB() {
		return is_96TB;
	}
	public void setIs_96TB(boolean is_96tb) {
		is_96TB = is_96tb;
	}
	public boolean isIs_120TB() {
		return is_120TB;
	}
	public void setIs_120TB(boolean is_120tb) {
		is_120TB = is_120tb;
	}
	public boolean isIs_144TB() {
		return is_144TB;
	}
	public void setIs_144TB(boolean is_144tb) {
		is_144TB = is_144tb;
	}
	public boolean isIs_3200TB() {
		return is_3200TB;
	}
	public void setIs_3200TB(boolean is_3200tb) {
		is_3200TB = is_3200tb;
	}
	public boolean isIs_6400TB() {
		return is_6400TB;
	}
	public void setIs_6400TB(boolean is_6400tb) {
		is_6400TB = is_6400tb;
	}
	public boolean isIs_7600TB() {
		return is_7600TB;
	}
	public void setIs_7600TB(boolean is_7600tb) {
		is_7600TB = is_7600tb;
	}
	public boolean isIs_12800TB() {
		return is_12800TB;
	}
	public void setIs_12800TB(boolean is_12800tb) {
		is_12800TB = is_12800tb;
	}
	public boolean isIs_15300TB() {
		return is_15300TB;
	}
	public void setIs_15300TB(boolean is_15300tb) {
		is_15300TB = is_15300tb;
	}
	public boolean isIs_30700TB() {
		return is_30700TB;
	}
	public void setIs_30700TB(boolean is_30700tb) {
		is_30700TB = is_30700tb;
	}
	public boolean isIs_61400TB() {
		return is_61400TB;
	}
	public void setIs_61400TB(boolean is_61400tb) {
		is_61400TB = is_61400tb;
	}


	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public String getFC() {
		return FC;
	}
	public void setFC(String fC) {
		FC = fC;
	}
	public String getOPT_PN() {
		return OPT_PN;
	}
	public void setOPT_PN(String oPT_PN) {
		OPT_PN = oPT_PN;
	}
}
