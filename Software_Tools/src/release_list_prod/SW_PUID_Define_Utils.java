package release_list_prod;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import common.Common_Utils;

public class SW_PUID_Define_Utils {

	@SuppressWarnings("deprecation")
	public void generate_PUID_Define(String path){
		XSSFWorkbook xssfWorkbook;
		try {
			xssfWorkbook = new XSSFWorkbook(path);
			StringBuffer stringBuffer = new StringBuffer();
			XSSFSheet xssfSheet = xssfWorkbook.getSheet("PUID_Define");
			if(xssfSheet==null){
				xssfSheet = xssfWorkbook.getSheetAt(2);
			}
			if(xssfSheet==null){
				System.out.println("The Sheet is not Exist");
			}
			//A 	B	C	D	E	F	G	H	I	J	K	L	L	M	N	O	P	Q	R
			//PUID	FC	Description	Class	CTO_PUID	ANN_Date	Country	IsSingleUse_1	IsSingleUse_More	Qty_Per_CPU	Qty_Always_1	FilterItem1	FilterItem2	FilterItem3	FilterItem4	FilterItem5	FilterItem6	FilterItem7	FilterItem8

			boolean isWindows =false;
			String cell0_String=xssfSheet.getRow(0).getCell(0).toString();
			if(cell0_String.contains("window")||cell0_String.contains("Window")||cell0_String.contains("WINDOW")){
				isWindows=true;
			}

			int FilterItem_start_Column=0;
			//if(isWindows){FilterItem_start_Column=19;}else{FilterItem_start_Column=13;}
			for(int i=0;i<xssfSheet.getRow(0).getLastCellNum()+1;i++)
			{
				String Column_Title=xssfSheet.getRow(0).getCell(i).toString();
				if(Column_Title.contains("FilterItem1")){
					FilterItem_start_Column=i;
					break;
				}
			}

			if(xssfSheet!=null){
				ArrayList<String>  PUID_List = new ArrayList<>();
				int eof_row= Common_Utils.getEOF_Row(xssfSheet, xssfSheet.getLastRowNum()+1);
				for(int i=1;i<eof_row;i++){
					XSSFRow row =xssfSheet.getRow(i);
					if(row==null) continue;
					int EOF_Column= Common_Utils.getEOF_Column(xssfSheet, i);
					//System.out.println("EOF_Column: "+EOF_Column);
					String PUID=row.getCell(0).toString();
					if(PUID.contains(".")){
						PUID=PUID.substring(0,PUID.lastIndexOf("."));
					}
					PUID_List.add(PUID);
					//				String FC=row.getCell(1).toString();
					//				String DESC=row.getCell(2).toString();
					//				String Class=row.getCell(3).toString();
					//String CTO_PUID=row.getCell(4).toString();
					//if(CTO_PUID.contains(".")){
					//	CTO_PUID=CTO_PUID.substring(0,CTO_PUID.lastIndexOf("."));
					//}
					//				String ANN_Date=row.getCell(5).toString();
					//				String SingleValue_1=row.getCell(6).toString();
					//				String SingleValue_2=row.getCell(7).toString();
					//				ArrayList<String>  FilterItemList = new ArrayList<>();
					String FilterItems="";
					for(int j=FilterItem_start_Column;j<EOF_Column;j++){
						if(j==EOF_Column-1){
							FilterItems = FilterItems+row.getCell(j).toString()+";";
						}else{
							FilterItems = FilterItems+row.getCell(j).toString()+",";
						}
					}
					//				puid 151002 extends IBMSPSS {
					//					 filterValues 	=   IBMSPSSModelerPremiumConcurrentUserFilterItem,BigData1yrsFilterItem;
					//					 pid			=	150974;
					//					 avail        	= 	av $Bid_Data_ANN_DATE:WW;
					//					 singleUseLicType = User;
					//					 validProducts   =	7X01,7X02,7X05,7X06;
					//				}
					stringBuffer.append("// "+PUID+"  "+row.getCell(1).toString()+"  "+row.getCell(2).toString()+"\n");
					stringBuffer.append("puid "+PUID+" extends  "+row.getCell(3).toString()+"{\n");
					stringBuffer.append("	filterValues       =    "+FilterItems+"\n");
					if(!row.getCell(4).toString().contains("N")){
						String Cell_String=row.getCell(4).toString();
						if(Cell_String.contains(".")){
							Cell_String=Cell_String.substring(0,Cell_String.lastIndexOf("."));
						}
						stringBuffer.append("	pid                =    "+Cell_String+";\n");
					}
					if(row.getCell(7).toString().contains("Y")){
						stringBuffer.append("	singleUseLicType   =    Instance;\n");	
					}
					if(row.getCell(8).toString().contains("Y")){
						stringBuffer.append("	singleUseLicType   =    User;\n");	
					}
					if(row.getCell(9).toString().contains("Y")){
						stringBuffer.append("	licenseType        =    perProcessor;\n");
						stringBuffer.append("	processorMax       =    1;\n");
						stringBuffer.append("	swInfoMessage      =    SW0001;\n");
						stringBuffer.append("	isVMWareSoftware   =    true;\n");	
					}
					if(row.getCell(10).toString().contains("Y")){
						stringBuffer.append("	licenseType        =    perProcessor;\n");
						stringBuffer.append("	processorMax       =    9999;\n");
						stringBuffer.append("	swInfoMessage      =    SW0002;\n");
						stringBuffer.append("	isVMWareSoftware   =    true;\n");	
					}
					if(!row.getCell(11).toString().contains("N")){
						String Max_String=row.getCell(11).toString();
						if(Max_String.contains(".")){
							Max_String=Max_String.substring(0,Max_String.lastIndexOf("."));
						}
						stringBuffer.append("	qtyMax             =    "+Max_String+";\n");
					}
					if(!isWindows){
						if(!row.getCell(12).toString().contains("N")){
							String Max_String=row.getCell(12).toString();
							if(Max_String.contains(".")){
								Max_String=Max_String.substring(0,Max_String.lastIndexOf("."));
							}
							int proc_value=0;
							if(Max_String.contains("1")){
								proc_value=1;
							}else if(Max_String.contains("2")){
								proc_value=2;
							}
							stringBuffer.append("	compatibleProcCount=    "+proc_value+";\n");
							stringBuffer.append("	swInfoMessage      =    000503;\n");
							//compatibleProcCount = 1;
							//swInfoMessage 	= 000503;
						}

						//nodeToLicenseQuantity	availOnClusterMode	years
						if(row.getCell(13).toString().contains("Y")){
							String Cell_String=row.getCell(13).toString();
							if(Cell_String.contains(".")){
								Cell_String=Cell_String.substring(0,Cell_String.lastIndexOf("."));
							}
							stringBuffer.append("	nodeToLicenseQuantity   =    1;\n");
						}

						if(!row.getCell(14).toString().contains("N")){
							String Cell_String=row.getCell(14).toString();
							if(Cell_String.contains(".")){
								Cell_String=Cell_String.substring(0,Cell_String.lastIndexOf("."));
							}
							stringBuffer.append("	years   				=    "+Cell_String+";\n");
							stringBuffer.append("	processorMax		    =    2;\n");
						}
						if(row.getCell(15).toString().contains("Y")){
							String Cell_String=row.getCell(15).toString();
							if(Cell_String.contains(".")){
								Cell_String=Cell_String.substring(0,Cell_String.lastIndexOf("."));
							}
							stringBuffer.append("	availOnClusterMode      =    Off;\n");
						}
						if(!row.getCell(16).toString().contains("N")){
							String Cell_String=row.getCell(16).toString();
							if(Cell_String.contains(".")){
								Cell_String=Cell_String.substring(0,Cell_String.lastIndexOf("."));
							}
							stringBuffer.append("	tierMax            =    "+Cell_String+";\n");
						}
					}

					//puid 121425 extends Windows2016R2OS {
					//    filterValues  = Win2016R2StdFilterItem, NoPreload_Win2016, EnglishOption_Win2016,MSOS2016To2012DowngradeMedia;
					//    hipoDerives   = 121428,121430;
					//    pidDerives    = 2303, 136796;
					//    mediaKit  	  = 121427;
					//    nodeToLicenseQuantity = 1;
					//	  coreMax		= 16;
					//    avail         = av:WW;
					//}
					if(isWindows){
						if(!row.getCell(12).toString().contains("N")){
							String Cell_String=row.getCell(12).toString();
							if(Cell_String.contains(".")){
								Cell_String=Cell_String.substring(0,Cell_String.lastIndexOf("."));
							}
							stringBuffer.append("	hipoDerives        =    "+Cell_String+";\n");
						}
						if(!row.getCell(13).toString().contains("N")){
							String Cell_String=row.getCell(13).toString();
							if(Cell_String.contains(".")){
								Cell_String=Cell_String.substring(0,Cell_String.lastIndexOf("."));
							}
							stringBuffer.append("	pidDerives         =    "+Cell_String+";\n");
						}
						if(!row.getCell(14).toString().contains("N")){
							String Cell_String=row.getCell(14).toString();
							if(Cell_String.contains(".")){
								Cell_String=Cell_String.substring(0,Cell_String.lastIndexOf("."));
							}
							stringBuffer.append("	mediaKit           =    "+Cell_String+";\n");
						}
						if(row.getCell(15).toString().contains("Y")){
							String Cell_String=row.getCell(15).toString();
							if(Cell_String.contains(".")){
								Cell_String=Cell_String.substring(0,Cell_String.lastIndexOf("."));
							}
							stringBuffer.append("	nodeToLicenseQuantity   =    1;\n");
						}
						if(row.getCell(16).toString().contains("Y")){
							String Cell_String=row.getCell(16).toString();
							if(Cell_String.contains(".")){
								Cell_String=Cell_String.substring(0,Cell_String.lastIndexOf("."));
							}
							stringBuffer.append("	coreMax            =    16;\n");
						}
						if(row.getCell(17).toString().contains("Y")){
							String Cell_String=row.getCell(17).toString();
							if(Cell_String.contains(".")){
								Cell_String=Cell_String.substring(0,Cell_String.lastIndexOf("."));
							}
							stringBuffer.append("	coreMin            =    17;\n");
						}
						if(row.getCell(18).toString().contains("Y")){
							String Cell_String=row.getCell(18).toString();
							if(Cell_String.contains(".")){
								Cell_String=Cell_String.substring(0,Cell_String.lastIndexOf("."));
							}
							stringBuffer.append("	licenseHipoDerives =    "+Cell_String+";\n");
						}
					}

					if(row.getCell(5).toString().contains("na")){
						stringBuffer.append("	avail              =    "+row.getCell(5).toString()+";\n");
					}else if(row.getCell(5).toString().contains("20")&&row.getCell(5).toString().contains("-")){
						stringBuffer.append("	avail              =    av "+row.getCell(5).toString()+":"+row.getCell(6).toString()+";\n");
					}else{
						stringBuffer.append("	avail              =    av $"+row.getCell(5).toString()+":"+row.getCell(6).toString()+";\n");	
					}

					stringBuffer.append("}\n");
				}
				if(PUID_List.size()>0){
					stringBuffer.append("\n\n"+PUID_List.toString()+"\n");
				}

				//Define  PUID DATA
				Define_PUID_List_For_Each_Class(stringBuffer, xssfSheet, eof_row);

			}


			//Define individual parts
			Define_Individual_Parts(xssfWorkbook, stringBuffer);
			//Define_Processors(xssfWorkbook, stringBuffer);

			//Save stringBuffer to Local
			saveDataToFile(stringBuffer.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public String get_Core_Number(String s){
		int index =0;
		int count=0;
		for(int i=0;i<s.length();i++){
			char a = s.charAt(i);
			String c =""+a;
			if(c.equals(" ")){
				count ++;
			}
			if(count==4){
				index=i;
				break;
			}
		}

		String z =s.substring(index).trim();
		//System.out.println(z);

		int t = z.indexOf(" ");
		//System.out.println(t);
		String x = z.substring(0, t-1).trim();
		//System.out.println(x);

		return x;
	}
	
	
	private void Define_Processors(XSSFWorkbook xssfWorkbook, StringBuffer stringBuffer) {
		XSSFSheet xssfSheet_LFO = xssfWorkbook.getSheet("PN");
		if(xssfSheet_LFO==null);
		xssfSheet_LFO =  xssfWorkbook.getSheetAt(3);
		System.out.println("Define_Infividual_Parts");
		if(xssfSheet_LFO==null){
			System.out.println("The Sheet is not Exist");
		}
		if(xssfSheet_LFO!=null){
			//System.out.println("Define_Infividual_Parts111");
			stringBuffer.append("\n\n");
			int eof_row1= Common_Utils.getEOF_Row(xssfSheet_LFO, xssfSheet_LFO.getLastRowNum()+1);
			if(eof_row1>2){
				for(int i=1;i<eof_row1;i++){
					//System.out.println("Define_Infividual_Parts222");
					XSSFRow row =xssfSheet_LFO.getRow(i);
					if(row==null) continue;
					String PUID=row.getCell(0).toString();
					if(PUID.contains(".")){
						PUID=PUID.substring(0,PUID.lastIndexOf("."));
					}
					String Core_Number = get_Core_Number(row.getCell(2).toString());
					//				String FC=row.getCell(1).toString();
					//				String DESC=row.getCell(2).toString();
					//				String PN=row.getCell(3).toString();
					//				String ANN_Date=row.getCell(4).toString();
					//				String Country=row.getCell(5).toString();

					//puid 161101 extends XeonSandyBridgeEP{//4XG7A14877 B4P4, SR650 Xeon Silver 4209T
					//	coreCount=8;
					//	preventAsLoosePart = $PREVENT_AS_LOOSE_PART_VALUE_FOR_20190312;
					//}
					stringBuffer.append("// "+PUID+"  "+row.getCell(1).toString()+"  "+row.getCell(3).toString()+"  "+row.getCell(2).toString()+"\n");
					stringBuffer.append("puid "+PUID+" extends  XeonSandyBridgeEP {\n");
					stringBuffer.append("	coreCount          =    "+Core_Number+";\n");
					stringBuffer.append("	avail              =    $"+row.getCell(4).toString()+";\n");
					stringBuffer.append("}\n");
				}
			}
		}
	}
	private void Define_Individual_Parts(XSSFWorkbook xssfWorkbook, StringBuffer stringBuffer) {
		XSSFSheet xssfSheet_LFO = xssfWorkbook.getSheet("PN");
		if(xssfSheet_LFO==null);
		xssfSheet_LFO =  xssfWorkbook.getSheetAt(3);
		System.out.println("Define_Infividual_Parts");
		if(xssfSheet_LFO==null){
			System.out.println("The Sheet is not Exist");
		}
		if(xssfSheet_LFO!=null){
			//System.out.println("Define_Infividual_Parts111");
			stringBuffer.append("\n\n");
			int eof_row1= Common_Utils.getEOF_Row(xssfSheet_LFO, xssfSheet_LFO.getLastRowNum()+1);
			if(eof_row1>2){
				for(int i=1;i<eof_row1;i++){
					//System.out.println("Define_Infividual_Parts222");
					XSSFRow row =xssfSheet_LFO.getRow(i);
					if(row==null) continue;
					String PUID=row.getCell(0).toString();
					if(PUID.contains(".")){
						PUID=PUID.substring(0,PUID.lastIndexOf("."));
					}
					//				String FC=row.getCell(1).toString();
					//				String DESC=row.getCell(2).toString();
					//				String PN=row.getCell(3).toString();
					//				String ANN_Date=row.getCell(4).toString();
					//				String Country=row.getCell(5).toString();
					//puid 153232 extends SoftwareItem {
					//	avail        	= 	av $Vmware_Embedded_ANN_DATE:CH;
					//}
					
					String PN=row.getCell(3).toString();
					if(PN.contains("N/A")){
						
					}else {
						
					}
					stringBuffer.append("// "+PUID+"  "+row.getCell(1).toString()+"  "+row.getCell(3).toString()+"  "+row.getCell(2).toString()+"\n");
					stringBuffer.append("puid "+PUID+" extends  SoftwareItem {\n");
					if(row.getCell(5).toString().contains("na")||row.getCell(4).toString().contains("na")){
						stringBuffer.append("	avail              =    "+row.getCell(5).toString()+";\n");
					}else if(row.getCell(4).toString().contains("20")&&row.getCell(4).toString().contains("-")){
						stringBuffer.append("	avail              =    av "+row.getCell(4).toString()+":"+row.getCell(5).toString()+";\n");
					}else{
						stringBuffer.append("	avail              =    av $"+row.getCell(4).toString()+":"+row.getCell(5).toString()+";\n");	
					}

					stringBuffer.append("}\n");
				}
			}
		}
	}

	private void Define_PUID_List_For_Each_Class(StringBuffer stringBuffer, XSSFSheet xssfSheet, int eof_row) {
		ArrayList<String> Class_String_List = new ArrayList<>();
		//int eof_row= Common_Utils.getEOF_Row(xssfSheet, xssfSheet.getLastRowNum()+1);
		for(int i =1;i<eof_row;i++){
			String Class = xssfSheet.getRow(i).getCell(3).toString();
			if(!Class_String_List.contains(Class)){
				Class_String_List.add(Class);
			}
		}
		//System.out.println("Class_String_List: "+Class_String_List.size()+"\n");

		for(int i =0;i<Class_String_List.size();i++){
			//System.out.println("KKKKKKK");
			String Class_String = Class_String_List.get(i);
			ArrayList<String> PUID_String_List = new ArrayList<>();
			for(int j =1;j<eof_row;j++){
				String Class = xssfSheet.getRow(j).getCell(3).toString();
				if(Class_String.equals(Class)){
					String PUID=xssfSheet.getRow(j).getCell(0).toString();
					if(PUID.contains(".")){
						PUID=PUID.substring(0,PUID.lastIndexOf("."));
					}
					PUID_String_List.add(PUID);
				}
			}
			String PUID_String = PUID_String_List.toString();
			PUID_String=PUID_String.substring(1,PUID_String.length()-1);

			stringBuffer.append("define "+Class_String+"Data "+ "{"+PUID_String+"}\n");
		}
	}

	public static void saveDataToFile (String string) {
		try {
			File file = new File("D:/1_SW/PUID_Define.txt");
			if (!file.exists()) {
				file.createNewFile();
			}
			FileOutputStream outStream = new FileOutputStream(file);
			outStream.write(string.trim().getBytes());
			outStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static int get_Column_from_Loadsheet(XSSFSheet xssfSheet,String flag_String) {
		int Flag_Column=0;
		XSSFRow row =xssfSheet.getRow(0);
		for(int i=0;i<row.getLastCellNum();i++){
			XSSFCell cell = row.getCell(i);
			if(cell==null) continue;
			String s=cell.toString();
			if(s.equals(flag_String)||s.contains(flag_String)){
				Flag_Column=i;
				System.out.println("get "+flag_String+" Column: "+i);
				return Flag_Column;
			}
		}
		return Flag_Column;
	}
}
