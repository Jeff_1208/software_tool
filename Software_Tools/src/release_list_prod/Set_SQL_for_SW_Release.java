package release_list_prod;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Set_SQL_for_SW_Release {

	//public static void main(String[] args) throws IOException{
	//	copyDataFromFile();
	//}


	public  void Genetate_SQL_For_PUID() throws IOException {
		
		File file = new File("D:/1_SW/Source_Release_List_Prod_PUID.txt");
		if (!file.exists()) {
			file.createNewFile();
			System.out.println("The Release_List_Prod_PUID.txt file is not exist, create the file, but the file is empty");
		}
		//InputStream得到字节输入流
		InputStream instream = new FileInputStream(file);
		StringBuilder Realse_content = new StringBuilder();
		
		StringBuilder List_Prod_content =new StringBuilder();
		
		if(instream.toString().isEmpty()) {
			System.out.println("The file is empty");
		}

		if (instream != null) {
			//InputStreamReader 得到字节流
			InputStreamReader inputreader = new InputStreamReader(instream);
			@SuppressWarnings("resource")
			//BufferedReader 是把字节流读取成字符流
			BufferedReader buffreader = new BufferedReader(inputreader);
			// 分行读取
			String line="";
			
			//insert into db2inst1.rel_prod (puid, ruid) select p.puid, r.ruid from db2inst1.product p, db2inst1.release r where p.puid =	148420	
			//and r.ruid in (select ruid from db2inst1.release where active = '1');
			
			//insert into DB2INST1.LIST_PROD (CID , PUID) values(999,	148587	);

			ArrayList<String> PUID_LIST= new ArrayList<>();
			while ((line = buffreader.readLine()) != null) {
				if(line.length()<1) break;
				String Each_Realse_SQL = "insert into db2inst1.rel_prod (puid, ruid) select p.puid, r.ruid from db2inst1.product p, db2inst1.release r where p.puid = "+line + " and r.ruid in (select ruid from db2inst1.release where active = '1' and ruid!=10020 and ruid!=12000 and ruid!=12242)\n";
				String Upcoming_Realse_SQL = "insert into DB2INST1.rel_prod (RUID , PUID) values(12335,"+line +")\n";
				Realse_content.append(Each_Realse_SQL);
				System.out.println(Upcoming_Realse_SQL);
				String Each_List_Prod_SQL = "insert into DB2INST1.LIST_PROD (CID , PUID) values(999,"+line +")\n";
				List_Prod_content.append(Each_List_Prod_SQL);
				//System.out.println(Each_List_Prod_SQL);
				PUID_LIST.add(line);
			}
			String PUID_list_String = PUID_LIST.toString();
			PUID_list_String =PUID_list_String.substring(1,PUID_list_String.length()-1);
			Realse_content.append("\n\n");
			List_Prod_content.append("\n\n");
			Realse_content.append("select * from DB2INST1.rel_prod where PUID in("+PUID_list_String +")\n");
			List_Prod_content.append("select * from DB2INST1.LIST_PROD where PUID in("+PUID_list_String +")\n");
			saveDataToFile(Realse_content.toString(),"D:/1_SW/Realse_table_SQL.txt");
			saveDataToFile(List_Prod_content.toString(),"D:/1_SW/List_Prod_table_SQL.txt");
		}
	}

	public  void saveDataToFile (String content, String path) {
		try {
			File file = new File(path);
			if (!file.exists()) {
				file.createNewFile();
			}
			FileOutputStream outStream = new FileOutputStream(file);
			outStream.write(content.trim().getBytes());
			outStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

