package software;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import product.Product_SQL;
import release_list_prod.SW_PUID_Define_Utils;
import release_list_prod.Set_SQL_for_SW_Release;

//Update
public class Software_Main_View extends JFrame implements ActionListener{

	private static final long serialVersionUID = 1L;

	static String Select_path;
	private JTextField CSR_Field;
	private JPanel mainpanel;
	private JPanel CTO_Name_panel;
	private JPanel showpanel;
	private JButton scan_btn_CSR;
	private JButton scan_btn_Loadsheet;
	private JButton run_btn;
	private JTextArea show_unusual_FC;

	static String CSR_path;
	public static String Update_Folder="Update";
	static String Source_CTO_Name;
	static String New_CTO_Name;
	static String Source_MT_Name;
	static String New_MT_Name;
	static String Source_Group_Start_MT;
	static String Source_Group_Start_CTO;
	static String New_Group_Start_MT;
	static String New_Group_Start_CTO;

	JRadioButton randioButton1;
	JRadioButton randioButton2;
	JRadioButton randioButton3;
	JRadioButton randioButton4;
	JRadioButton randioButton5;

	public Software_Main_View() {

		show_unusual_FC= new JTextArea(10,30);
		show_unusual_FC.setLineWrap(true);  

		CSR_Field = new JTextField(20);

		scan_btn_CSR = new JButton("Find Source File ");
		scan_btn_Loadsheet = new JButton("Find Source Loadsheet ");
		run_btn = new JButton("Start Run");
		run_btn.setSize(10,30);

		randioButton1=new JRadioButton("Generate SQL Create PUID",true);  
		randioButton2=new JRadioButton("Generate PUID Define"); 
		randioButton3=new JRadioButton("Generate FilterItem");  
		randioButton4=new JRadioButton("Generate SQL Update Description");  
		randioButton5=new JRadioButton("Generate SQL for Release&List_Prod"); 
		ButtonGroup group=new ButtonGroup();  
		group.add(randioButton1);  
		group.add(randioButton2);
		group.add(randioButton3);
		group.add(randioButton4);
		group.add(randioButton5);
		CTO_Name_panel = new JPanel();
		CTO_Name_panel.setLayout(new GridLayout(8,1,1,1)) ;
		CTO_Name_panel.add(scan_btn_CSR);
		CTO_Name_panel.add(CSR_Field);
		CTO_Name_panel.add(randioButton1);
		CTO_Name_panel.add(randioButton5);
		CTO_Name_panel.add(randioButton3);
		CTO_Name_panel.add(randioButton2);
		CTO_Name_panel.add(randioButton4);
		CTO_Name_panel.add(run_btn);

		showpanel = new JPanel();
		mainpanel = new JPanel();
		mainpanel.setLayout(new BorderLayout());
		showpanel.setLayout(new BorderLayout());
		showpanel.add(show_unusual_FC);
		mainpanel.add(CTO_Name_panel,BorderLayout.NORTH); 
		mainpanel.add(showpanel,BorderLayout.SOUTH); 
		setTitle("Software Tool");

		this.add(mainpanel, BorderLayout.CENTER); 
		this.setSize(600, 1000);
		this.setVisible(true);
		scan_btn_CSR.addActionListener(this);
		scan_btn_Loadsheet.addActionListener(this);
		run_btn.addActionListener(this);
		this.addWindowListener(new WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});

		Toolkit too = Toolkit.getDefaultToolkit() ;
		Dimension screenSize = too.getScreenSize() ;
		this.setLocation((screenSize.width-WIDTH)/2, (screenSize.height-HEIGHT)/2) ;
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE) ;
		this.setResizable(true); 
		this.pack();
	}


	public static void main(String[] args) {
		new Software_Main_View();

	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if(e.getSource()==scan_btn_CSR){
			JFileChooser chooser = new JFileChooser();
			chooser.setMultiSelectionEnabled(false);

			chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			chooser.setDialogTitle("Find Source File");


			int result = chooser.showOpenDialog(this);
			if (result == JFileChooser.APPROVE_OPTION) {
				File file = chooser.getSelectedFile();
				String filepath = file.getAbsolutePath();
				CSR_Field.setText( filepath);// 将文件路径设到JTextField
				CSR_path=filepath;
			}
		}else if(e.getSource()==run_btn){
			Product_SQL product_SQL = new Product_SQL(CSR_path);

			//randioButton1=new JRadioButton("Generate SQL Create PUID",true);  
			//randioButton2=new JRadioButton("Generate PUID Define"); 
			//randioButton3=new JRadioButton("Generate FilterItem");  
			//randioButton4=new JRadioButton("Generate SQL Update Description");  
			if(randioButton1.isSelected()){
				product_SQL.generate_SQL_create_PUID(CSR_path);
				show_unusual_FC.setText("Please find the file for Create PUID SQL from D:/1_SW/Software_SQL_Create_PUID.txt");
			}else if(randioButton2.isSelected()){
				SW_PUID_Define_Utils sw_PUID_Define_Utils =new SW_PUID_Define_Utils();
				sw_PUID_Define_Utils.generate_PUID_Define(CSR_path);
				show_unusual_FC.setText("Please find the file for PUID Define from D:/1_SW/PUID_Define.txt");
			}else if(randioButton3.isSelected()){
				product_SQL.generate_Category_List_Filter_Item(CSR_path);
				show_unusual_FC.setText("Please find the file for FilterItem from D:/1_SW/Generate_FilterItem.txt");
			}else if(randioButton4.isSelected()){
				product_SQL.generate_SQL_Update_Description(CSR_path);
				show_unusual_FC.setText("Please find the file for update Description from D:/1_SW/SQL_For_SW_Update_Description.txt");
			}else if(randioButton5.isSelected()){
				//product_SQL.generate_SQL_Update_Description(CSR_path);
				Set_SQL_for_SW_Release set_SQL_for_SW_Release = new Set_SQL_for_SW_Release();
				try {
					set_SQL_for_SW_Release.Genetate_SQL_For_PUID();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				show_unusual_FC.setText("Please find the file for update Description from D:/1_SW/List_Prod_table_SQL.txt and D:/1_SW/Realse_table_SQL.txt");
			}
			//			product_SQL.generate_CTO_FC_Item(CSR_path);
			//			product_SQL.set_map_for_Service(CSR_path);
			//			product_SQL.get_PUID_DB_for_Source_File(CSR_path);
			//			product_SQL.generate_AML_Code_for_Service(CSR_path);

		}

		//String unusual_FC_message="Please find the CTO Loadsheet from:\n"+CSR_path+"//"+Update_Folder;
		//show_unusual_FC.setText(unusual_FC_message);
	}

}

