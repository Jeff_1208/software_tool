package other;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class CombineFC4 {

	public static void main(String[] args) throws IOException{
		copyDataFromFile();
		//saveDataToFile (content);
	}
	private static String copyDataFromFile() throws IOException {
		File file1 = new File("D:/FC3.txt");
		String txt1 = ReadTxt(file1);
		String[] ss1= txt1.split(",", 0);
		
		File file2 = new File("D:/CompareFC3.txt");
		String txt2 = ReadTxt(file2);
		String[] ss2= txt2.split(",", 0);

		List<String> ss3= new ArrayList<String>();
		
		for(int i=0;i<ss2.length;i++){
			boolean flag =false;
			for(int j=0;j<ss1.length;j++){
				if(ss2[i].equals(ss1[j])){
					flag=true;
				}
			}
			if(!flag){
				ss3.add(ss2[i]);
			}
		}

		String content = "";
		for(int i=0;i<ss3.size();i++){
			content+=ss3.get(i).trim()+",";
		}
		System.out.println(content);
		return content;
	}

	private static String ReadTxt(File file) throws FileNotFoundException,IOException {
		@SuppressWarnings("resource")
		FileReader reader = new FileReader(file);
		int fileLen = (int)file.length();
		char[] chars = new char[fileLen];
		reader.read(chars);
		String txt = String.valueOf(chars);
		System.out.println(txt);
		//System.out.println(txt);
		if(!txt.contains(",")){
			txt = ReadTxt2(file);
			System.out.println("kkkkkk");
		}
		return txt;
	}

	private static String ReadTxt2(File file) throws FileNotFoundException,IOException {
		//InputStream得到字节输入流
		@SuppressWarnings("resource")
		InputStream instream = new FileInputStream(file);
		String content = "";

		if(instream.toString().isEmpty()) {
			System.out.println("The file is empty");
		}

		if (instream != null) {
			//InputStreamReader 得到字节流
			InputStreamReader inputreader = new InputStreamReader(instream);
			@SuppressWarnings("resource")
			//BufferedReader 是把字节流读取成字符流
			BufferedReader buffreader = new BufferedReader(inputreader);

			// 分行读取
			String line="";
			while ((line = buffreader.readLine()) != null) {
				content += line + ",";
				//content += "'"+line + "',";
			}
			//content= SortForFCs(content);//FC 排序
			System.out.println("Combine the FC result is:"+content.trim());
		}
		return content;
	}

	public static void saveDataToFile (String string) {
		File file = new File("D:/CombineFC.txt");
		try {
			if(!file.exists()){
				file.createNewFile();
			}
			if(string!=null){
				FileOutputStream fileOutputStream = new FileOutputStream(file);
				fileOutputStream.write(string.getBytes());
				fileOutputStream.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}

