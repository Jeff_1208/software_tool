package other;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class CombineFC2 {

	public static void main(String[] args) throws IOException{
		String content = copyDataFromFile();
		saveDataToFile (content);
	}

	//	 关于InputStream.read(byte[] b)和InputStream.read(byte[] b,int off,int len)这两个方法都是用来从流里读取多个字节的，
	//	有经验的程序员就会发现，这两个方法经常 读取不到自己想要读取的个数的字节。
	//	比如第一个方法，程序员往往希望程序能读取到b.length个字节，而实际情况是，系统往往读取不了这么多。
	//	仔细阅读Java的API说明就发现了，这个方法 并不保证能读取这么多个字节，它只能保证最多读取这么多个字节(最少1个)。
	//	因此，如果要让程序读取count个字节，最好用以下代码：
	//	  byte[] b = new byte[count];
	//	  int readCount = 0; // 已经成功读取的字节的个数
	//	  while (readCount < count) {
	//	   readCount += in.read(bytes, readCount, count - readCount);
	//	  }

	private static String copyDataFromFile() throws IOException {
		/*		File file = new File("D:/FC.txt");
		if(!file.exists()){
			file.createNewFile();
		}
		String content ="";
		InputStream inStream=  new FileInputStream(file);
		if(inStream!=null){
			InputStreamReader inputStreamReader = new InputStreamReader(inStream);
			BufferedReader bufferedReader =new BufferedReader(inputStreamReader);

			String line="";
			while((line=bufferedReader.readLine())!=null){
				content+=line+",";

			}
		}
		content= SortForFCs(content);
		return content;
		 */
		File file = new File("D:/FC2.txt");
		if (!file.exists()) {
			file.createNewFile();
			System.out.println("The FC2.txt file is not exist, create the file, but the file is empty");
		}
		//InputStream得到字节输入流
		InputStream instream = new FileInputStream(file);
		String content = "";

		if(instream.toString().isEmpty()) {
			System.out.println("The file is empty");
		}

		if (instream != null) {
			//InputStreamReader 得到字节流
			InputStreamReader inputreader = new InputStreamReader(instream);
			@SuppressWarnings("resource")
			//BufferedReader 是把字节流读取成字符流
			BufferedReader buffreader = new BufferedReader(inputreader);

			// 分行读取
			String line="";
			while ((line = buffreader.readLine()) != null) {
				//content += line + ",";
				content += "'"+line + "',";
				//content += "\""+line + "\",";
			}
			//content= SortForFCs(content);//FC 排序
			content=content.substring(0,content.length()-1);
			//System.out.println("Combine the FC result is: select * from DB2INST1.PRODUCT  where FEAT_CODE in("+content.trim()+")");
			System.out.println("select * from DB2INST1.PRODUCT  where FEAT_CODE in("+content.trim()+")");
			//select * from DB2INST1.product where CAT_ID = 321 and feat_code in
			//System.out.println("select * from DB2INST1.product where CAT_ID = 321 and feat_code in ("+content.trim()+")");
		}
		return content;
	}

	public static void saveDataToFile (String string) {
		/*
		File file = new File("D:/CombineFC.txt");
		try {
			if(!file.exists()){
				file.createNewFile();
				if(string!=null){
					FileOutputStream fileOutputStream = new FileOutputStream(file);
					fileOutputStream.write(string.getBytes());
					fileOutputStream.close();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
		 */
		
		try {
			File file = new File("D:/CombineFC2.txt");

			if (!file.exists()) {
				file.createNewFile();
			}
			FileOutputStream outStream = new FileOutputStream(file);
			outStream.write(string.trim().getBytes());
			outStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static String SortForFCs(String string) throws IOException {
		List<String> FCs= new ArrayList<String>();

		String[] s=string.split(",");
		for(int i=0;i<s.length;i++){
			//System.out.println("The file is empty"+s[i]);
			FCs.add(s[i]);
		}
		Comparator<String> comp = new ComparatorFC();
		Collections.sort(FCs, comp);
		StringBuilder sb = new StringBuilder();
		
		//String str="";
		for(int j=0;j<FCs.size();j++){
			sb.append(FCs.get(j)+",");
			//str +=FCs.get(j)+",";
		}
		return sb.toString();
	}

}

