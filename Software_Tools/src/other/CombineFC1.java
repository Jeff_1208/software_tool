package other;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;

public class CombineFC1 {

	public static void main(String[] args) throws IOException{
		copyDataFromFile();
		//saveDataToFile (content);
	}

	//	 关于InputStream.read(byte[] b)和InputStream.read(byte[] b,int off,int len)这两个方法都是用来从流里读取多个字节的，
	//	有经验的程序员就会发现，这两个方法经常 读取不到自己想要读取的个数的字节。
	//	比如第一个方法，程序员往往希望程序能读取到b.length个字节，而实际情况是，系统往往读取不了这么多。
	//	仔细阅读Java的API说明就发现了，这个方法 并不保证能读取这么多个字节，它只能保证最多读取这么多个字节(最少1个)。
	//	因此，如果要让程序读取count个字节，最好用以下代码：
	//	  byte[] b = new byte[count];
	//	  int readCount = 0; // 已经成功读取的字节的个数
	//	  while (readCount < count) {
	//	   readCount += in.read(bytes, readCount, count - readCount);
	//	  }

	private static String copyDataFromFile() throws IOException {

		File file = new File("D:/FC1.txt");
		if (!file.exists()) {
			file.createNewFile();
			System.out.println("The FC.txt file is not exist, create the file, but the file is empty");
		}

		@SuppressWarnings("resource")
		FileReader reader = new FileReader(file);
		int fileLen = (int)file.length();
		char[] chars = new char[fileLen];
		reader.read(chars);
		String txt = String.valueOf(chars);
		System.out.println(txt);

		String content="";

		if (txt != null) {
			String[] ss= txt.split(",", 0);
			for(int i=0;i<ss.length;i++){
				//System.out.println("ss.lngth"+ss.length);
				//System.out.println("ss+["+i+"] is "+ss[i]);
				//System.out.println(""+ss[i]);
				content+=ss[i].trim()+"\r";
			}
		}
		System.out.println(content);

		try {
			File file1 = new File("D:/CombineFC1.txt");
			if (!file1.exists()) {
				file1.createNewFile();
			}
			FileOutputStream outStream = new FileOutputStream(file1);
			outStream.write(content.getBytes());
			outStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return content;
	}

	public static void saveDataToFile (String string) {
		File file = new File("D:/CombineFC.txt");
		try {
			if(!file.exists()){
				file.createNewFile();
			}
			if(string!=null){
				FileOutputStream fileOutputStream = new FileOutputStream(file);
				fileOutputStream.write(string.getBytes());
				fileOutputStream.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}



