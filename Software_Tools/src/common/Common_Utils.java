package common;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
//BBBAAA
public class Common_Utils {

	public static int getEOF_Row(XSSFSheet xssfSheet, int total_row) {
		int end_index=0;
		for(int i=0;i<total_row;i++){
			XSSFRow row =xssfSheet.getRow(i);
			if(row==null) continue;
			XSSFCell cell = row.getCell(0);
			if(cell==null) continue;
			String s=cell.toString();
			if(s.equals("#EOF")){
				end_index=i;
				break;
			}
		}
		if(end_index==0){
			end_index=total_row;
		}
		return end_index;
	}
	public static int getEOF_Row_public(Sheet xssfSheet, int total_row) {
		int end_index=0;
		for(int i=0;i<total_row;i++){
			Row row =xssfSheet.getRow(i);
			if(row==null) continue;
			Cell cell = row.getCell(0);
			if(cell==null) continue;
			String s=cell.toString();
			if(s.equals("#EOF")){
				end_index=i;
				break;
			}
		}
		if(end_index==0){
			end_index=total_row;
		}
		return end_index;
	}
	public static int getEOF_Row_For_Each_Column(XSSFSheet xssfSheet,int Column) {
		int end_index=0;
		//System.out.println("Last row: "+xssfSheet.getLastRowNum());
		for(int i=0;i<xssfSheet.getLastRowNum()+2;i++){
			XSSFRow row =xssfSheet.getRow(i);
			if(row==null){
				end_index=i;
				//System.out.println("KKK");
				break;
			}
			XSSFCell cell = row.getCell(Column);
			if(cell==null){
				end_index=i;
				//System.out.println("ZZZ "+i+" Column: "+Column);
				break;
			}
			String s=cell.toString();
			if(s.length()<1){
				end_index=i;
				//System.out.println("YYY");
				break;
			}
		}
		return end_index;
	}
	

	public static int getEOF_Column(XSSFSheet xssfSheet, int initial_row) {
		int end_index=0;

		XSSFRow row =xssfSheet.getRow(initial_row);
		for(int i=0;i<=row.getLastCellNum()+2;i++){
			XSSFCell cell = row.getCell(i);
			if(cell==null){
				end_index=i;
				break;
			}
			String s=cell.toString();
			if(s.length()<1){
				end_index=i;
				break;
			}
		}
		return end_index;
	}
}
